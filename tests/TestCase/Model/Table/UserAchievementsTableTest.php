<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserAchievementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserAchievementsTable Test Case
 */
class UserAchievementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserAchievementsTable
     */
    public $UserAchievements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_achievements',
        'app.users',
        'app.owned_items',
        'app.ratings',
        'app.items',
        'app.reserved_items',
        'app.achievements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserAchievements') ? [] : ['className' => 'App\Model\Table\UserAchievementsTable'];
        $this->UserAchievements = TableRegistry::get('UserAchievements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserAchievements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
