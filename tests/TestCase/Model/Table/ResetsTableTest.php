<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResetsTable Test Case
 */
class ResetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ResetsTable
     */
    public $Resets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.resets',
        'app.users',
        'app.owned_items',
        'app.reserved_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Resets') ? [] : ['className' => 'App\Model\Table\ResetsTable'];
        $this->Resets = TableRegistry::get('Resets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Resets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
