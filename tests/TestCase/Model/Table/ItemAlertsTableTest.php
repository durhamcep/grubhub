<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemAlertsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemAlertsTable Test Case
 */
class ItemAlertsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemAlertsTable
     */
    public $ItemAlerts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_alerts',
        'app.users',
        'app.owned_items',
        'app.ratings',
        'app.items',
        'app.reserved_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemAlerts') ? [] : ['className' => 'App\Model\Table\ItemAlertsTable'];
        $this->ItemAlerts = TableRegistry::get('ItemAlerts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemAlerts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
