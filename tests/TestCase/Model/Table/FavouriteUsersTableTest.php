<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FavouriteUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FavouriteUsersTable Test Case
 */
class FavouriteUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FavouriteUsersTable
     */
    public $FavouriteUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.favourite_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FavouriteUsers') ? [] : ['className' => 'App\Model\Table\FavouriteUsersTable'];
        $this->FavouriteUsers = TableRegistry::get('FavouriteUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FavouriteUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
