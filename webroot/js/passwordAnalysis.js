
function analysePassword(var password)
{	
	
	var passwordStrength = 0;
	var lengthGain = 0.3 * password.length;
	
	var noOfUpper = 0;
	var noOfLower = 0;
	var numbers = "0123456789";
	var containsNumbers = false;
	for(i = 0; i< password.length;i++)
	{
		
		if(password.charAt(i)== password.charAt(i).toUpperCase())
		{
			noOfUpper+=1; 
		}
		else
		{
			noOfLower+=1;
		}
		if(numbers.indexOf(password.charAt(i))> -1)
		{
			containsNumbers = true;
		}
		
	}
	
	var AltChar = Math.floor(((noOfUpper) * (noOfLower))^0.2);
	if(containsNumbers)
	{
		AltChar = (AltChar*2 + 1);
	}
	
	passwordStrength = lengthGain + AltChar;
	
	if(passwordStrength <10)
	{
		return "Weak Password";
	}
	else if(passwordStrength <20)
	{
		return "Medium Password";
	}
	else if(passwordStrength <30)
	{
		return "Strong Password";
	}
	else
	{
		return "Very Strong Password";
	}
}
