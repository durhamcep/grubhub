# Installation

Do the following:

```
$ composer install
$ cp config/app.php.default config/app.php
```

Edit `config/app.php`, particularly the database values.

```
$ bin/cake migrations migrate
$ bin/cake server
```

# Routes

| Route             | Description             |
| ----------------- | ----------------------- |
| /users            | View a list of all users |
| /users/add        | Create a new user |
| /users/edit/id    | Edit user with id id |
| /users/login      | Login the user          |
| /users/logout     | Logout the user         |
| /users/view/id    | View user with id id    |
| /items            | View a list of items |
| /items/add        | Add an item |
| /items/edit/id    | Edit item with id id |
| /items/view/id    | View item with id id |

# Git tutorial
If you start working on a feature you will need to create and then checkout a new branch:

```
$ git checkout -b feature-name
```

You can always check what branch you're on. Eg. after the above:

```
$ git branch
feature-name
```

When you have made some changes you need to add them to the staging area and commit them. Commits should be small. For example if I was implementing user login I'd do a commit after writing the code to make the database table, a commit for adding a new user, a commit for editing users and so on.

To see what files have changed do the following:

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	composer.phar

no changes added to commit (use "git add" and/or "git commit -a")
```

Untracked files are files that git has not seen before. If none of your changes are untracked then you can do:

```
$ git commit -a
```

to commit them. This will open an editor where you write a commit message. Generally this should be a short summary on the first line (~50 chars) and then if more information is needed a blank line and then the rest of the information (~79 chars per line). For example:

```
Fix messaging so you only see messages between the two users

Before any message that was sent or received by the current logged in user would
be displayed when viewing a message. Really we want to show sent by <other_user>
and received by <current_user> or sent by <current_user> and received by
<other_user>.

TLDR: Matt is an idiot.
```

If you have an untracked file you need to add it. 

```
$ git add path/to/untracked/file
```

And then commit:

```
$ git commit
```

If you want to upload the code to gitlab, do:

```
$ git push origin
```

This will push the current branch to gitlab
