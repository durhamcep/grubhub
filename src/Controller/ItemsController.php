<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Network\Http\Client;
use Cake\Datasource\ConnectionManager;


/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 */
class ItemsController extends AppController
{	
    //initialise vars
	var $gotLocation=false;
	var $geo;
	var $lat;
	var $long;
	var $dist=20;
	public $helpers = array('GoogleMap');   //GMaps helper

	//initialise page
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['index', 'view']);
    }
    
	//checks if user is authorised
    public function isAuthorized($user) {
        if (in_array($this->request->params['action'], ['edit', 'delete'])) {
            $item = $this->Items->get($this->request->params['pass'][0]);
            return $item->user_id == $user['id'];
			//if authorised return userId
        }
        return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
		//Checks user is authorised
		$this->set('auser', $this->Auth->user());
		$searchTerm=null;
		
		//Sets us post/put requests
		if ($this->request->is('post') ||$this->request->is('put'))
		{
			$searchTerm=$this->request->data['searchTerm'];
		}		
		
		//initialise vars/set up distances for lat/long
		$dist=20;
		$lat=$this->Auth->user('lat');
		$long=$this->Auth->user('lon');
		$this->set('lat',$lat);
		$this->set('long',$long);
		// div by 69.1 lat->miles
		// div by 53.0 long->miles
		// * by 69.1 miles->lat
		// * by 53.0 miles->long
        $lath=$lat+($dist/69.1); //Max dist North
		$latl=$lat-($dist/69.1); //Max dist South
		$lonh=$long+($dist/53.0); //Max dist East
		$lonl=$long-($dist/53.0); //Max dist West
		$gotLocation=true;
		$date = date("Y-m-d");
		
		//connection manager to connect to SQL server
		ConnectionManager::config('my_connection', [
			'className' => 'Cake\Database\Connection',
			'driver' => 'Cake\Database\Driver\Mysql',
			'persistent' => false,
			'host' => 'localhost',
			'username' => 'grubhub',
			'password' => 'grubhub', //change to your password
			'database' => 'grubhub',
			'encoding' => 'utf8',
			'timezone' => 'UTC',
			'cacheMetadata' => true,
		]);
		
		if($searchTerm==null) //If they haven't specified a search
		{
			$items = $this->Items->find('all',[ //Give all 
								 'contain'=>['Users'],
								 'conditions'=>[
									'reserved' => 0, //not reserved
									'expires >=' => $date, //not expired
									'Users.lat <' => $lath, //Within 'dist' miles East
									'Users.lat >' => $latl, //West
									'Users.lon <' => $lonh, //North
									'Users.lon >' => $lonl]]); //South
		}
		else
		{
			$items = $this->Items->find('all',[
								 'contain'=>['Users'],
								 'conditions'=>[
									'reserved' => 0, //not reserved
									'expires >=' => $date, //not expired
									'Users.lat <' => $lath, //Within 'dist' miles East
									'Users.lat >' => $latl, //West
									'Users.lon <' => $lonh, //North 
									'Users.lon >' => $lonl, //South
									'OR' => ['Items.name LIKE'=> '%'.$searchTerm.'%', //Where either the name 
									         'Items.description LIKE'=> '%'.$searchTerm.'%', //The description
											 'Items.food_type LIKE'=> '%'.$searchTerm.'%']]]); //Or the food type contains the searchTerm
																							   
								 
		}
		
		
		foreach ($items as $item)
		{	
			
			
			
            $item->distace = pow(pow($lat-$item->lat*69.1, 2) + pow($long-$item->lon*53.0, 2), 0.5); //Generates a distance from the user to the item
			//debug($item->user->lat);
		}
		$items = $this->paginate($items); //Sends the items to the template and displays them
		$this->set('location');
		$this->set(compact('items'));
		$this->set('_serialize', ['items']);
	
		
    }

	public function rate($id) {
		$item = $this->Items->get($id); //Get id of item to be rated
		$this->loadModel('Ratings');
		if ($this->request->is('post') ||$this->request->is('put')) { //if they're rating it
		$rating = $this->Ratings->newEntity([ //save rating
			'item_id' => $id, 
			'score' => $this->request->data['score']
		]);
		if ($this->Ratings->save($rating)) { //if the rating is being saved
			
			
			if($this->request->data['score'] == 5) //Check to see if its 5*
			{
				$conn = ConnectionManager::get('default');
				//count number of 5* ratings
				$numberOf5Stars = $conn->execute('SELECT COUNT(`ratings`.`id`) as NumberOfFives 
												  FROM `ratings` INNER JOIN `items` on `ratings`.`item_id` = `items`.`id`
												  WHERE `ratings`.`score` = 5 AND `items`.`user_id` = ?',[$item->user_id])->fetch("assoc");
				if($numberOf5Stars['NumberOfFives'] == 1) //If first 5* rating
				{
					//give the achievement
					$this->loadModel('UserAchievements');
					$x = $this->UserAchievements->newEntity(['user_id' => $item->user_id, 'achievement_id' => 3]);
				
					$this->UserAchievements->save($x);
					$this->set('success', true);
				}
				
			}
			
			$this->set('success', true);
			
			
		} else {
			$this->set('success', false);
		}
		}
	}
    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
		$this->set('auser', $this->Auth->user());
        $item = $this->Items->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->isBlocked($item->user->id, $this->Auth->user('id'))) { //checks if you are blocked by the person who has the item
            $this->redirect('/items'); //sends you back to the find page
            $this->Flash->error('You are not allowed to interact with this person');
            return;
        }
        if ($item->user_id != $this->Auth->user('id') && $item->reserved && $item->reserved_by != $this->Auth->user('id')) { //checks its not your item
            $this->Flash->error('You are not allowed to view this item');
            $this->redirect(['action' => 'index']); //sends you back to the find page
        }
        $this->set('item', $item);
        $this->set('user_id', $this->Auth->user('id'));
        $this->set('_serialize', ['item']);
    }

	/**
	 *Search method
	*/
	public function search($searchTerm)
	{    	
		$this->Flash->success('It worked');
	}
	
	public function getItems()
	{
		if($gotLocation)
		{
			
		}
	}
	
    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {

		$conn = ConnectionManager::get('default');

		$this->set('auser', $this->Auth->user());
        $id = $this->Auth->user('id');

		$query = $this->Items->find();
        $common_items = $query->select(['count' => $query->func()->count('name'), //find the top 4 most frequent items
                                        'name' => 'name'])
                              ->where(['user_id' => $this->Auth->user('id')])
                              ->group('name')
                              ->order(['count' => 'DESC'])
                              ->limit(4);
		
        $items = [];
        foreach ($common_items as $common_item) { // for each common item
            if ($common_item['count'] >= 1) {
                $items[] = $common_item; //add to array
            }
        }
		$itemInfo=[];
		foreach ($items as $item) //for each common item
		{
			$query = $this->Items->find();
			$info=$query->select(['id'=>'id','name'=>'name', 'barcode'=>'barcode','description'=>'description','type'=>'type'])
						->where(['name'=>$item['name']])
						->first(); //get all its info 
			array_push($itemInfo,$info); //add to array
		}
		$this->set('frequentItems', $common_items); //set arrays so they can be used by the template
		$this->set('itemInfo',$itemInfo);
		
        $item = $this->Items->newEntity(); //create new item
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['reserved'] = false;
            $this->request->data['reserved_by'] = null;
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) { //add item to data base
                $this->Flash->success(__('The item has been saved.'));
				

                $this->loadModel('ItemAlerts');
                $this->loadModel('Notifications'); //if the item has been added as an alert
                $alerts = $this->ItemAlerts->query()->where(['item_name' => $item->name]);
                foreach ($alerts as $alert) {
                    $notif = $this->Notifications->newEntity([ //create the notification
                        'ntype' => 'item_alert',
                        'item_id' => $item->id,
                        'user_id' => $alert->user_id,
                        'message_id' => null,
                        'been_read' => false,
                    ]);
                    $this->Notifications->save($notif);
                }

				$conn = ConnectionManager::get('default');
		
				//if the user has been favourited
				$notificationList = $conn->execute('SELECT user 
							  FROM favourite_users
							  WHERE favourited_user = ?',[$this->Auth->user('id')])->fetchAll("assoc");
		

				foreach($notificationList as $row)
				{
					$this->loadModel('Notifications'); //create notification
			
					$notif = $this->Notifications->NewEntity([
						'ntype' => "favourite_user_posted_item",
						'item_id'=>$item->id,
						'user_id' => $row['user'],
						'message_id' => null,
						'been_read' => false
					]);
					$test = "made it in";
					if($this->Notifications->save($notif))
					{
					}
				}	

				//get number of items the user has given away
				$numberOfItems = $conn->execute('SELECT COUNT(`items`.`id`) as countItems 
												 FROM `items`
												 WHERE `items`.`user_id` = ?',[$this->request->data['user_id']])->fetch("assoc");
												 
												// $this->Auth->user('id')])->fetch("assoc");
				//achievements				
				
				if($numberOfItems['countItems'] == 1) //if this is the 1st item
				{
					$this->loadModel('UserAchievements');
					$x = $this->UserAchievements->newEntity(['user_id' => $this->Auth->user('id'), 'achievement_id' => 1]);
					$this->UserAchievements->save($x); //give achievement
				}
				elseif($numberOfItems['countItems'] == 10) //if this is the 10th item
				{
					$this->loadModel('UserAchievements'); //give achievement
					$x = $this->UserAchievements->newEntity(['user_id' => $this->Auth->user('id'), 'achievement_id' => 2]);
					$this->UserAchievements->save($x);
				}
				
				
				
                //return $this->redirect(['action' => 'index']);
				return $this->redirect(['action' => 'view',$item->id]);
            }
			
            $this->Flash->error(__('The item could not be saved. Please, try again.'));
        }
        $users = $this->Items->Users->find('list', ['limit' => 200]);
        $items = $this->Items->findByUserId($id);
        $this->set(compact('item', 'users'));
        $this->set('_serialize', ['item']);
        $this->set(compact('user','user'));
        $this->set(compact('items','items'));
		
			
    }

    /**
     * Edit method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->set('auser', $this->Auth->user()); 
        $item = $this->Items->get($id, [ //get id of item being edited
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) { 
            $item = $this->Items->patchEntity($item, $this->request->data); //get the edited data
            if ($this->Items->save($item)) {  //save the new data
                $this->Flash->success(__('The item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item could not be saved. Please, try again.'));
        }
        $users = $this->Items->Users->find('list', ['limit' => 200]);
        $this->set(compact('item', 'users'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $item = $this->Items->get($id); //get id of the item to be deleted
        if ($this->Items->delete($item)) { //delete item
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']); //redirect to the find page
    }

    public function reserve($id) {
        $this->loadModel('Chats');
        $this->request->allowMethod(['post']);
        $item = $this->Items->get($id, ['contain' => ['Users']]); //if the item is being reserved
        if ($this->isBlocked($item->user->id, $this->Auth->user('id'))) { //check you've not blocked each other
            $this->redirect('/items');
            $this->Flash->error('You are not allowed to interact with this person');
            return;
        }
        if ($item->user_id == $this->Auth->user('id')) { //check its not your item
            $this->Flash->error('You cannot reserve your own item');
            $this->redirect(['action' => 'view', $id]);
            return;
        }
        if ($item->reserved) { //check its not already reserved
            $this->Flash->error('This item is already reserved');
            $this->redirect(['action' => 'view', $id]);
            return;
        }
        $item->reserved = true; //set the item as being reserved
        $item->reserved_by = $this->Auth->user('id'); 
		//generate a chat between the two users 
        $chat = $this->Chats->newEntity(['item_owner' => $item->user_id, 'reserver' => $this->Auth->user('id'), 'item_id' => $item->id, 'closed' => false]);
        
        if ($this->Items->save($item)) { //save the item update (reserve)
            if ($this->Chats->save($chat)) { //save chat
                $this->Flash->success('Successfully reserved your item. Please message the user who posted the item');
                $this->redirect(['controller' => 'chats', 'action' => 'view', $chat->id]);
            } else { //if error
                $item->reserved = false; //un reserver everything
                $item->reserved_by = null;
                $this->Items->save($item);
                $this->Flash->error('Could not reserve this item. Please try again later');
                $this->redirect(['action' => 'view', $id]);
            }
        } else {
            $this->Flash->error('Could not reserve this item. Please try again later');
            $this->redirect(['action' => 'view', $id]);
        }
    }

    public function unreserve($id) {
        $this->request->allowMethod(['post']);
        $item = $this->Items->get($id);
        $user_id = $this->Auth->user('id');
        if ($item->reserved_by != $user_id && $item->user_id != $user_id) { //check user either created or reserved item
            $this->Flash->error('You cannot unreserve an item you did not create or reserve');
            $this->redirect($this->referer());
            return;
        }
        $item->reserved = false; //unreserve item
        $item->reserved_by = null;
        if (!$this->Items->save($item)) {
            $this->Flash->error('Could not unreserve the item, please try again later');
            $this->redirect($this->referer());
        } else {
            $this->Flash->success('The item is no longer reserved');
            $this->redirect(['action' => 'index']);
        }
    }

    public function claimed($id) { //set item as claimed
        $this->request->allowMethod(['post']);
        $item = $this->Items->get($id);
        $user_id = $this->Auth->user('id');
        if ($item->user_id != $user_id) { //check its the user who made the item
            $this->Flash->error('You are not authorised to do that');
            $this->redirect($this->referer());
            return;
        }
        $item->claimed = true; //save the item as being claimed
        if (!$this->Items->save($item)) {
            $this->Flash->error('Could not set this item as claimed, please try again later');
            $this->redirect($this->referer());
        } else {

            $this->Flash->success('The item has been claimed.');
            $this->loadModel('Notifications');
			//set notification for item being claimed
            $notification = $this->Notifications->newEntity([
                'ntype' => 'claimed_item',
                'item_id' => $id,
                'been_read' => false,
                'user_id' => $item->reserved_by,
            ]);
            $this->Notifications->save($notification);

            $this->redirect(['action' => 'index']);
        }
    }
	
	
	private function getLatLonForPostcode($postcode) {
        $http = new Client();
        $response = $http->get("http://api.postcodes.io/postcodes/$postcode"); //postcode api
        $json = $response->json;

		// return ['lat' => 53, 'lon' => 1];
        return ['lat' => $json['result']['latitude'], 'lon' => $json['result']['longitude']]; 

    }
	
	private function distanceBetween($loc1, $loc2)
	{
		$x=($loc1->lat)-($loc2->lat);
		$y=($loc1->lon)-($loc2->lon);
		return sqrt($x^2 +$y^2);
	}
}
