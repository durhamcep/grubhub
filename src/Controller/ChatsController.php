<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Expression\IdentifierExpression;

/**
 * Chats Controller
 *
 * @property \App\Model\Table\ChatsTable $Chats
 */
class ChatsController extends AppController
{
	//loads the model
    public function initialize() {
        parent::initialize();
        $this->loadModel('Messages');
    }
	//checks the user is authorized/logged in
    public function isAuthorized($user) {
        if (in_array($this->request->params['action'], ['index', 'notifications'])) {
            return true;
        }
        $user_id = $user['id'];
        $chat = $this->Chats->get($this->request->params['pass'][0]);
        return $chat->item_owner == $user_id || $chat->reserver == $user_id;
    }
	//main chat page functionality, finds all messages with a given user and sets them up for view
    public function index() {
		//authorise
		$this->set('auser', $this->Auth->user());
		//get user
        $user_id = $this->Auth->user('id');
		//get chats
		$query = $this->Chats->find()
            ->contain(['Items', 'ItemOwner', 'Reserver'])
            ->where(['OR' => ['item_owner' => $user_id,  'reserver' => $user_id], 'closed' => false]);
        //get messages
		foreach ($query as $chat) {
            $message = $this->Messages->find()
                     ->where(['chat_id' => $chat->id])
                     ->order('created desc')
                     ->first();
            $chat->message = $message;
        }
		//send chats array forward
        $this->set('chats', $query);
        $this->set('user_id', $user_id);
    }

    public function close($id) {
		//end a chat update DB
        $chat = $this->Chats->get($id);
        $chat->closed = true;
        $this->Chats->save($chat);
    }

    public function view($id)
    {
		//authorise
		$this->set('auser', $this->Auth->user());
        //get user
		$user_id = $this->Auth->user('id');
        //get the specific chat
		$chat = $this->Chats->get($id, ['contain' => ['Items', 'ItemOwner', 'Reserver']]);
        //load model for items
		$this->loadModel('Items');
        $item = $this->Items->get($chat->item_id);
        //get all messages for this chat
		$messages = $this->Messages->find()
                    ->where(['chat_id' => $id])
                    ->order(['Messages.created' => 'ASC'])->toArray();
        //prepare to send notifications
		$this->loadModel('Notifications');
        foreach($messages as $message) {
            //checks if the message has been read, if so save it and set notification to read (default is not read)
			if ($message->mto == $user_id && !$message->been_read) {
                $message->been_read = true;
                $this->Messages->save($message);
                $notification = $this->Notifications->find()
                              ->where(['user_id' => $user_id,
                                       'message_id' => $message->id])->first();
                $notification->been_read = true;
                $this->Notifications->save($notification);
            }
        }
		//sets information up about the other member of the chat
        $reserver = $chat->reserver == $user_id;
        $this->set('chat', $chat);
        $this->set('item', $item);
        $this->set('messages', $messages);
        $this->set('user_id', $user_id);
        $this->set('reserver', $reserver);
        $this->set('id', $id);
		//set up pictures
        if ($reserver) {
            $this->set('our_pic', $chat->from->photo);
            $this->set('their_pic', $chat->to->photo);
        } else {
            $this->set('our_pic', $chat->to->photo);
            $this->set('their_pic', $chat->from->photo);
        }
    }
	//sends new messages to the chats page also handles some notification
    public function poll($id)
    {
        $chat = $this->Chats->get($id);
        $user_id = $this->Auth->user('id');
        $messages = $this->Messages->find()
                  ->where(['chat_id' => $id, 'mto' => $user_id, 'been_read' => false])
                  ->order(['created' => 'ASC'])->toArray();
        $this->loadModel('Notifications');
        foreach($messages as $message) {
            $message->been_read = true;
            $this->Messages->save($message);
            $notification = $this->Notifications->find()
                          ->where(['user_id' => $user_id,
                                   'message_id' => $message->id])->first();
            $notification->been_read = true;
            $this->Notifications->save($notification);
        }
        $this->set('messages', $messages);
    }
	//sends a message
    public function send($id) {
		//choose chat and recipient
        $chat = $this->Chats->get($id);
        $to = $chat->reserver;
        if ($to == $this->Auth->user('id')) {
            $to = $chat->item_owner;
        }
        if ($this->request->is('post')||$this->request->is('put')) {
          //get text and save a message
			$text = $this->request->data["message"];
            $message = $this->Messages->newEntity([

				'chat_id' => $id,
                'mto' => $to,
                'message' => $text,
                'been_read' => false,
            ]);
			//ensures that the message is saved and then loads and updates notifications
            if ($this->Messages->save($message)) {
                $this->loadModel('Notifications');
                $notification = $this->Notifications->newEntity([
                    'user_id' => $to,
                    'ntype' => 'message',
                    'message_id' => $message->id,
                    'been_read' => false,
                ]);
				//ensures the the notification is saved
                if ($this->Notifications->save($notification)) {
                    $this->set('success', true);
                } else {
                    $this->set('success', false);
                    $this->set('errors', $notification->errors());
                }
            } else {
                $this->set('success', false);
                $this->set('errors', $message->errors());
            }
        }
    }
}
