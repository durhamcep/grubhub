<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Http\Client;

use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\Mailer\Email;
use Cake\I18n\Time;




/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
	 //controller for the home page
    public function index()
    {
        $users = $this->paginate($this->Users);
        $this->set('auser', $this->Auth->user());
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
        $this->loadModel('Notifications');
		//queries all of the unread notifications and displays them
        $unread_notifications = $this->Notifications->find()
                       ->contain(['Messages', 'Messages.Chats', 'Messages.Chats.ItemOwner', 'Messages.Chats.Reserver', 'Items'])
                       ->where([
                           'Notifications.user_id' => $this->Auth->user('id'),
                           'Notifications.been_read' => false])
                       ->order('Notifications.created DESC');
		//queries all of the read notifications and displays them
        $read_notifications = $this->Notifications->find()
                            ->contain(['Messages', 'Messages.Chats', 'Messages.Chats.ItemOwner', 'Messages.Chats.Reserver', 'Items'])
                            ->where([
                                'Notifications.user_id' => $this->Auth->user('id'),
                                'Notifications.been_read' => true])
                            ->order('Notifications.created DESC');
		//sets all of the unread notifiactions to read
        foreach($unread_notifications as $notification) {
            $notification->read = $notification->been_read;
            if (!$notification->been_read) {
                $notification->been_read = true;
                $this->Notifications->save($notification);
            }
        }
		//passes the arrays to the view
        $this->set('unread_notifications', $unread_notifications);
        $this->set('read_notifications', $read_notifications);
        $this->set('auser', $this->Auth->user());
    }

	
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	 //profile page controller
    public function view($id = null)
    {
		//sets up the query connection manager
		$conn = ConnectionManager::get('default');
        if ($this->isBlocked($this->Auth->user('id'), $id)) {
            $this->Flash->error('You are not allowed to interact with this person');
            $this->redirect('/items');
            return;
        }
		$this->set('auser', $this->Auth->user());
        $user = $this->Users->get($id, [
            'contain' => ['ReservedItems', 'OwnedItems', 'ReservedItems.Users']
        ]);

		//load and query the ratings table from model
        $this->loadModel('Ratings');
        foreach($user->reserved_items as $item) {
            $item['rating'] = $this->Ratings->find()
                            ->where(['item_id' => $item->id])
                            ->first();
        }
		//calculate this users average rating
        $this->set('auth_user', $this->Auth->user());
        $this->set('user', $user);
		$rating = $conn->execute('SELECT AVG(score) AS a 
								FROM `ratings` INNER JOIN `items` ON `ratings`.`item_id` = `items`.`id`
								WHERE `items`.`user_id` = ?',[$id])->fetch("assoc");
		if($rating['a'] == null)
		{
			$rating['a'] = "No Ratings";
		}
		$this->set('rating', $rating['a']);
		
		$conn = ConnectionManager::get('default');
		//get all of a users gained achievements and send to the view
		$achievementTable = $conn->execute('SELECT `achievements`.`name` AS achievementname, `achievements`.`score` AS achievementscore
											FROM `achievements` INNER JOIN `user_achievements` ON `achievements`.`id` = `user_achievements`.`achievement_id`
											WHERE `user_achievements`.`user_id` = ?',[$id])->fetchAll("assoc");								
		$this->set("GA",$achievementTable);
		
		//query the model for the information about items to get the frequently added items
        $this->loadModel('Items');
        $query = $this->Items->find();
        $common_items = $query->select(['count' => $query->func()->count('name'),
                                        'name' => 'name'])
                              ->where(['user_id' => $this->Auth->user('id')])
                              ->group('name')
                              ->order(['count' => 'DESC'])
                              ->limit(3);
        $items = [];
        foreach ($common_items as $item) {
            if ($item['count'] >= 1) {
                $items[] = $item;
            }
        }
        $this->set('auth_user', $this->Auth->user());
        $this->set('user', $user);
        $this->set('items', $items);
        $this->set(compact('users'));
        $this->set('_serialize', ['user']);
    }
	//check the postcode information, if not valid return null else return the lat and long of the postcode
    private function getLatLonForPostcode($postcode) {
        $http = new Client();
        $response = $http->get("http://api.postcodes.io/postcodes/$postcode");
        $json = $response->json;
		
		if(!array_key_exists('result',$json))
		{
			return ['lat' => null, 'lon' => null];
		}
		// return ['lat' => 53, 'lon' => 1];
        return ['lat' => $json['result']['latitude'], 'lon' => $json['result']['longitude']];

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
	 
	 //the create user functionality
    public function add()
    {
		//pull info from the user inputs and do some validation to ensure that all fields are correct
		$this->set('auser', $this->Auth->user());
        $user = $this->Users->newEntity();
        if ($this->request->is('post')||$this->request->is('put')) {
			$user = $this->Users->patchEntity($user, $this->request->data);
			$latLonArr = $this->getLatLonForPostcode($user->postcode);
			//postcode check
			if(($latLonArr['lat'] != null) && ($latLonArr['lon'] != null))
			{
				
				$geo = $this->getLatLonForPostcode($user->postcode);
				$user->lat = $geo['lat'];
				$user->lon = $geo['lon'];
				if ($this->Users->save($user)) {
					$this->Flash->success(__('The user has been saved.'));

					return $this->redirect(['action' => 'index']);
				}
			}
			else
			{
				$this->set(compact('user'));
				return;
			}
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
		//create user
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
	//functionality to update/edit a user
	 public function edit($id = null)
    {
		//same as the add user functionality
		$this->set('auser', $this->Auth->user());
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $geo = $this->getLatLonForPostcode($user->postcode);
            $user->lat = $geo['lat'];
            $user->lon = $geo['lon'];
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	 
	 //functionality to delete the user
    public function delete($id = null)
    {
		//send the delete request
        $this->request->allowMethod(['post', 'delete']);
		//choose user and delete them,save database
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
		//send back to homepage (and realistically log in page)
        return $this->redirect(['action' => 'index']);
    }
	//login functionality
    public function login()
    {
		//cakephp authorisation for username and password
		$this->set('auser', $this->Auth->user());
        if ($this->request->is('post') ||$this->request->is('put')){
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'users', 'action' => 'index']);
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }
	//initializes the below params. cake magic.
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'logout', 'reset', 'key', 'privacy', 'contact', 'halloffame']);
    }

    public function logout()
    {
		//cake logout and redirect
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
	//reset password
    public function reset() {
        if ($this->request->is('post')||$this->request->is('put')) {
			//regex keys
            $chars ='1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
            $key = "";
            for ($i=0; $i < 16; $i++) {
                $key .= $chars[rand(0, strlen($chars)-1)];
            }
            $this->loadModel('Resets');
			//find the correct email and send a password key to it
            $user = $this->Users->find()->where(['email' => $this->request->data['email']])->first();
            //invalid email input
			if ($user == null) {
                $this->Flash->error('Email not found');
                return;
            }
			//tracks the reset request in the database
            $reset = $this->Resets->newEntity(['user_id' => $user->id, 'secret' => $key]);
            $this->Resets->save($reset);
            $email = new Email('default');
			//send the email
            $email->to($user->email)
                  ->setSubject('GrubGub password reset')
                  ->send('Someone has requested to reset your password on grubhub. Please go to http://grubhub.com/users/key/' . $key . ' to reset your password');
            $this->Flash->success('Reset code sent');
            $this->redirect('/');
        }
    }
	//actuallly reset functionality - once the email key has been used
    public function key($key=null) {
        $this->loadModel('Resets');
        $user = $this->Users->newEntity();
		//find the user and the reset request and ensure not expired (after 1 day)
        $this->set('user', $user);
        $reset = $this->Resets->find()
               ->contain(['Users'])
               ->where(['secret' => $key,
                        'created >=' => new Time('-1 day')])
               ->first();
			   
		//assuming failure
        if ($reset == null) {
            $this->Flash->error('Could not find that reset code. Maybe it has expired');
            $this->redirect('/');
            return;
        }
		//request new password and update in model - redirects to login
        if ($this->request->is('post')||$this->request->is('put')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $this->set('user', $user);
            if (!$user->errors('password')) {
                $user = $reset->user;
                $user->password = $this->request->data['password'];
                $this->Users->save($user);
                $this->Flash->success('Password updated. Please login');
				//redirect
                $this->redirect(['controller' => 'users', 'action' => 'login']);
				//remove reset request from the database
                $this->Resets->delete($reset);
            }
        }
    }

    public function notifications() {
		//load notifications
        $this->loadModel('Notifications');
		//query for notifications of types chats,reserved,favourite
        $notifications = $this->Notifications->find()
                       ->contain(['Messages', 'Messages.Chats', 'Messages.Chats.ItemOwner', 'Messages.Chats.Reserver', 'Items'])
                       ->where(['Notifications.user_id' => $this->Auth->user('id')])
                       ->order('Notifications.created DESC');
        foreach($notifications as $notification) {
            $notification->read = $notification->been_read;
			//updates notifications from not read to read
            if (!$notification->been_read) {
                $notification->been_read = true;
                $this->Notifications->save($notification);
            }
        }
		//send the notifications to the model
        $this->set('notifications', $notifications);
		//sets the current user
        $this->set('auser', $this->Auth->user());
    }
	//cake magic
    public function isAuthorized($user) {
        return true;
    }
	//ability to favourite a user
	public function favourite()
	{
		
		$this->loadModel('FavouriteUsers');
		if($this->request->is('post')||$this->request->is('put'))
		{
			//adds a favourite user pair to the database
			$favourite = $this->FavouriteUsers->NewEntity([
				'user' => $this->request->data['usr'],
				'favourited_user' => $this->request->data['fav']
		]);
			if($this->FavouriteUsers->save($favourite))
			{
				$this->set('success',true);
			}
			else
			{
				$this->set('success',false);
			}
		}
	}
	
	public function unfavourite()
	{
		//query to remove a user's favourite user from the database
		$conn = ConnectionManager::get('default');
		$updatedTable = $conn->execute('DELETE FROM favourite_users
										WHERE user = ? AND favourited_user = ?',[$this->request->data['usr'],$this->request->data['fav']]);
		
			//currently unwritten
	}

	
	public function halloffame()
	{
		
		$conn = ConnectionManager::get('default');
		//query to get the users with the best total scores (based on achievements and no.of items posted and average rating)
		$ratings = $conn->execute('SELECT `users`.username As userName, AVG(ratings.score) AS a, ((AVG(ratings.score)*AVG(ratings.score)*COUNT(items.id))/COUNT(user_achievements.user_id)) + SUM(achievements.score) as scr 
								  FROM `ratings` INNER JOIN `items` ON `ratings`.`item_id` = `items`.`id` 
                                  				 INNER JOIN `users` ON `items`.`user_id` = `users`.`id`
                                  				 INNER JOIN `user_achievements` ON `items`.`user_id` = `user_achievements`.`user_id` 
                                                 INNER JOIN `achievements` ON `achievements`.`id` = `user_achievements`.`achievement_id`
												 GROUP BY `items`.`user_id`
												 ORDER BY  scr DESC
												 LIMIT 5')->fetchAll("assoc");
												 
		$this->set("halloffame",$ratings);
	}
	//nada
	public function getAchievements()
	{
	}

	public function privacy()
    {
		$this->set('auser', $this->Auth->user());
	}
	public function contact()
    {
		$this->set('auser', $this->Auth->user());
	}
	//loads blocks and adds a user pair to it.
    public function block($id = null) {
        $this->loadModel('Blocks');
        $block = $this->Blocks->newEntity([
            'blocker' => $this->Auth->user('id'),
            'blockee' => $id
        ]);
        $this->Blocks->save($block);
        $this->Flash->success('The user has been blocked');
		//redirects to item page
        $this->redirect(['controller' => 'Items', 'action' => 'index']);
    }

    public function alerts() {
		//load user and alert table
        $this->set('auser', $this->Auth->user());
        $this->loadModel('ItemAlerts');
		//query table for all items that the user has set alerts on for
        $alerts = $this->ItemAlerts->query()->where(['user_id' => $this->Auth->user('id')]);
        //add new entry to alerts
		$a = $this->ItemAlerts->newEntity();
        $this->set('a', $a);
        $this->set('alerts', $alerts);
        if ($this->request->is('post') ||$this->request->is('put')){
            $alert = $this->ItemAlerts->newEntity(['user_id' => $this->Auth->user('id')]);
            $alert = $this->ItemAlerts->patchEntity($alert, $this->request->data);
            $this->ItemAlerts->save($alert);
        }
    }

    public function deletealert($id) {
		//load the item alerts, get the alert to be deleted, remove from table, redirect to alerts page
        $this->loadModel('ItemAlerts');
        $alert = $this->ItemAlerts->get($id);
        $this->ItemAlerts->delete($alert);
        $this->Flash->success('The alert has been deleted');
        $this->redirect(['action' => 'alerts']);
    }
}
