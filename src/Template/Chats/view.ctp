<?php
/**
  * @var \App\View\AppView $this
  */
?>
<head>
    <!-- bootstrap framework -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('LoggedInTemplate.css') ?>
    <?= $this->Html->css('chatView.css') ?>

</head>

<body class="chatView">
	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

<div style="height: 10vh; background-color: #9cd944;"></div>

<div class="chatHeading">
	<h1>Chat</h1>
</div>
<div class="userAndItem">
<h3>
    <?php if ($chat->item_owner == $user_id): ?>
        Sharing with <?= $chat->from->username ?>
    <?php else: ?>
        Collecting from <?= $chat->to->username ?>
    <?php endif ?>
</h3>

<h4><?= $item->name?></h4>
</div>
<div class="messages">
    <?php
    foreach($messages as $message) {
        if ($message->mto != $user_id) {
            echo "<div class='our-message'>";
            if ($our_pic) { echo "<img src='/files/Users/photo/$our_pic'></img>"; }
            echo  "<div class='messageDiv'>Us: $message->message</div></div>";
        } else {
            echo "<div class='their-message'>";
            if ($their_pic) { echo "<img src='/files/Users/photo/$their_pic'></img>"; }
            echo "<div class='messageDiv'>Them: $message->message</div></div>";
        }
    }
    ?>
</div>
<div class="formsAndMessages">
<form id="message-form">
    <input type="text" id="message-input" placeholder="message"/>
    <div id="submitButton">
      <input type="submit" value="Send"/>
    </div>
</form>

<br/>
<br/>

<?php
if ($reserver) {
    echo $this->Form->create(null, ['class' => 'close-form', 'url' => ['controller' => 'items', 'action' => 'unreserve', $item->id]]);
    echo $this->Form->submit('Unreserve', ['id'=>'unreserveButton']);
    echo $this->Form->end();
} else {
    echo $this->Form->create(null, ['class' => 'close-form', 'url' => ['controller' => 'items', 'action' => 'claimed', $item->id]]);
    echo $this->Form->submit('The item has been given away');
    echo $this->Form->end();

    echo $this->Form->create(null, ['class' => 'close-form', 'url' => ['controller' => 'items', 'action' => 'unreserve', $item->id]]);
    echo $this->Form->submit('This user can\'t claim the item');
    echo $this->Form->end();
}
?>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>

<script type="text/javascript">
 $("#message-form").submit(function() {
     var text = $("#message-input").val();
     if (text == "") {
         return false;
     }
     $.ajax({
         type: "POST",
         url: "/chats/send/<?= $id ?>",
         data: {message: text},
         dataType: "json",
         success: function() {
             var div = $("<div class='our-message'></div>");
             <?php if ($our_pic): ?>
                div.append("<img src='/files/Users/photo/<?= $our_pic ?>'></img>");
             <?php endif ?>
             div.append("<div class='messageDiv'>Us: " + text + "</div>");
             $(".messages").append(div);
             $("#message-input").val("");
         }
     });
     return false;
 });

 $(".close-form").submit(function() {
   $.ajax({
    type: "POST",
    url: "/chats/close/<?= $id ?>",
    contentType: "json",
    async: false});
   return true;
 });

function poll() {
     $.getJSON('/chats/poll/<?= $id ?>', function(data) {
         $(data.messages).each(function(i, message) {
             var div = $("<div class='their-message'></div>");
             <?php if ($their_pic): ?>
                div.append("<img src='/files/Users/photo/<?= $their_pic ?>'></img>");
             <?php endif ?>
             div.append("Them: " + message.message);
             $(".messages").append(div);
         });
     });
     window.setTimeout(poll, 1000);
}
poll();
</script>

  <!-- Bottom nav-bar -->
  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	<div class="navButton">
		<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	<div class="title">
		<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	</div>
	<div class="navButton">
		<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	</div>
  </nav>

</body>
