<?php
/**
  * @var \App\View\AppView $this
  */
?>
<head>
    <!-- bootstrap framework -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('LoggedInTemplate.css') ?>
    <?= $this->Html->css('chatIndex.css') ?>

</head>

<body class="chatIndex">
	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

<div style="height: 10vh; background-color: #9cd944;"></div>
<div class="chatBody">
<div class="chatHeading">
	<div style="width: 25vw;"></div>
	<h1>Chat</h1>
</div>

<ul>

    <div class="messageTitle">Unread</div>
    <?php $unreadMessageCount = 0?>
    <?php foreach ($chats as $chat): ?>
        <?php if (!$chat->message || !$chat->message->been_read): ?>
            <?php if (!$chat->message || $chat->message->mto == $user_id): ?>
                <!-- Whether we are sharing or collecting the item -->
                <?php $unreadMessageCount = $unreadMessageCount + 1?>
                <div class="chatChat">

                    <div class="chatType chatInsides">
                        <?php if ($chat->item_owner == $user_id): ?>
                            Sharing
                        <?php else: ?>
                            Collecting
                        <?php endif ?>
                    </div>
                    <!-- Information for the other user (name and picture) -->
                    <div class="chatOtherUser chatInsides">
                        <div class="chatUserImage">
                            <?php if ($chat->to->id == $user_id): ?>
                                <div class="chatUserName"><?= $chat->from->username ?></div>
                                <?php if ($chat->from->photo): ?>
                                    <img src="<?= '/files/Users/photo/' . $chat->from->photo ?>"></img>
                                <?php else:?>
                                    <div class="noImageDiv"></div>
                                <?php endif ?>

                            <?php else: ?>
                                <div class="chatUserName"><?= $chat->to->username ?></div>
                                <?php if ($chat->to->photo): ?>
                                    <img src="<?= '/files/Users/photo/' . $chat->to->photo ?>"></img>
                                  <?php else:?>
                                      <div class="noImageDiv"></div>
                                  <?php endif ?>

                            <?php endif ?>
                        </div>
                    </div>
                    <div class="itemImage">
                        <?php if ($chat->item->photo_dir): ?>
                            <div class="chatInsides">Item Image</div>
                            <img src="<?= '/files/Items/photo/' . $chat->item->photo ?>"></img>
                        <?php endif ?>
                    </div>

                    <!-- Information -->
                    <div class="chatInformation chatInsides">
                        Food: <?= $chat->item->name ?> <br/>
                        Expires: <?= h($chat->item->expires) ?>
                    </div>
                    <div class="chatLastMessage chatInsides">
                        <?php if ($chat->message): ?>
                            <!--
                            <?php if ($chat->message->mto != $user_id): ?>
                            You
                            <?php else: ?>
                            <?= $chat->from->username ?>
                            <?php endif ?>:
                            -->
                            <?= $chat->message->message ?>
                        <?php endif ?>
                    </div>
                    <!-- View link -->

                    <div class="chatInsides">
                        <?php if ($chat->to->id == $user_id): ?>
                            <li class="chatInsides"><?= $this->Html->link('View More', ['action' => 'view', $chat->id]) ?></li>
                        <?php else: ?>
                            <li class="chatInsides"><?= $this->Html->link('View More', ['action' => 'view', $chat->id]) ?></li>
                        <?php endif ?>
                    </div>

                </div>
                <?php endif?>
            <?php endif ?>
    <?php endforeach ?>
    <?php if($unreadMessageCount==0): ?>
        <div class="noMessages"> No Unread Messages</div>
    <?php endif ?>

    <div class="messageTitle">Read</div>
    <?php $readMessageCount = 0?>
    <?php foreach ($chats as $chat): ?>
        <?php if ($chat->message && ($chat->message->been_read || (!$chat->message->been_read && $chat->message->mto != $user_id))): ?>
            <?php $readMessageCount = $readMessageCount + 1?>
            <!-- Whether we are sharing or collecting the item -->
            <div class="chatChat">

                <div class="chatType chatInsides">
                    <?php if ($chat->item_owner == $user_id): ?>
                        Sharing
                    <?php else: ?>
                        Collecting
                    <?php endif ?>
                </div>
                <!-- Information for the other user (name and picture) -->
                <div class="chatOtherUser chatInsides">
                    <div class="chatUserImage">
                        <?php if ($chat->to->id == $user_id): ?>
                            <div class="chatUserName"><?= $chat->from->username ?></div>
                            <?php if ($chat->from->photo): ?>
                                <img src="<?= '/files/Users/photo/' . $chat->from->photo ?>"></img>
                            <?php else:?>
                                <div class="noImageDiv"></div>
                            <?php endif ?>

                        <?php else: ?>
                            <div class="chatUserName"><?= $chat->to->username ?></div>
                            <?php if ($chat->to->photo): ?>
                                <img src="<?= '/files/Users/photo/' . $chat->to->photo ?>"></img>
                            <?php else:?>
                                <div class="noImageDiv"></div>
                            <?php endif ?>

                        <?php endif ?>
                    </div>
                </div>
                <div class="itemImage">
                    <?php if ($chat->item->photo_dir): ?>
                        <div class="chatInsides">Item Image</div>
                        <img src="<?= '/files/Items/photo/' . $chat->item->photo ?>"></img>
                    <?php endif ?>
                </div>

                <!-- Information -->
                <div class="chatInformation chatInsides">
                    Food: <?= $chat->item->name ?> <br/>
                    Expires: <?= h($chat->item->expires) ?>
                </div>
                <div class="chatLastMessage chatInsides">
                    <!--
                    <?php if ($chat->message->mto != $user_id): ?>
                    You
                    <?php else: ?>
                    <?= $chat->from->username ?>
                    <?php endif ?>:
                    -->
                    <?= $chat->message->message ?>
                </div>
                <!-- View link -->

                <div class="chatInsides">
                    <?php if ($chat->to->id == $user_id): ?>
                        <li class="chatInsides"><?= $this->Html->link('View More', ['action' => 'view', $chat->id]) ?></li>
                    <?php else: ?>
                        <li class="chatInsides"><?= $this->Html->link('View More', ['action' => 'view', $chat->id]) ?></li>
                    <?php endif ?>
                </div>

            </div>
            <?php endif?>
    <?php endforeach ?>

    <?php if($readMessageCount==0): ?>
        <div class="noMessages"> No Read Messages </div>
    <?php endif ?>

</ul>
</div>
<!-- Bottom nav-bar -->
<nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	  <div class="navButton">
		    <?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	  <div class="title">
		    <?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	  </div>
	  <div class="navButton">
		    <?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	  </div>
</nav>
</body>
