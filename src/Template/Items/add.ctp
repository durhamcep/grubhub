
<?php
/**
  * @var \App\View\AppView $this
  */
?>
<head>
  <?= $this->Html->css('LoggedInTemplate.css') ?>
  <?= $this->Html->css('itemAdd.css') ?>
<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>
  <!-- navbar when signed in-->

	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

  <!-- spacing for navbar -->

  <div style="height: 10vh; background-color: #9cd944;"></div>
  <!-- green heading bar -->
  <div class="allItems">
  <div class="addHeading">


    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Sort by
        <span class="caret"></span>
      </button>

      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li id="allButton">All</li>
        <li id="currentButton">Current</li>
        <li id="expiredButton">Expired</li>
        <li id="claimedButton">Claimed</li>
      </ul>

    </div>

    <h1>Give</h1>
    <button class="btn toggleAddButton" type="button">
		Add Items
	</button>
  </div>

<div class="greyGivePanel">
    <button class="btn gotTheBarcode" type="button">
		Got the barcode?
	</button>

<!-- Barcode -->
<div class = "barcodeStuff">
	 <p class = "cameraPane" id = "camera"></p> <!-- this is not the camera it just wants this -->
	  <input type = "button" value = "scan barcode live" id = "scanButtonLive" onclick = "readBarcodeLive()">
	  <form>

	<input type = "button" value = "scan by picture" id = "scanButtonPicture" onclick = "setLiveFalse()">
	<input type="file" id = "barcodePicture" accept="image/*;capture=camera"/>
	</form>
</div>
<!-- End of barcode  -->



	<div class="frequent items frequentItemsTitle">
			<?php
				$fItems=['frequentItems'];

				foreach ($frequentItems as $item)
				{
					array_push($fItems, $item['name']);
				}
				echo $this->Form->input('Item',array('type'=>'select','options'=>$fItems, 'id'=>'itemsList'));
				echo $this->Form->button('Add Frequent Item',array('id'=>'frequentItems'));
			?>
      </div>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
			<script type='text/javascript'>
			    var itemInfo = [];
				<?php foreach ($itemInfo as $ii): ?>
				    var info = {};
					info.name = "<?= $ii->name; ?>"
					info.barcode = "<?= $ii->barcode; ?>"
					info.description = "<?= $ii->description; ?>"
					info.type = "<?= $ii->type; ?>"
					itemInfo.push(info);
				<?php endforeach; ?>
				$('#frequentItems').click(function addItem()
				{
					console.log($('#itemsList').val());
					$('#barcode').val(itemInfo[$('#itemsList').val()-1]['barcode']);
					$('#name').val(itemInfo[$('#itemsList').val()-1]['name']);
					$('#description').val(itemInfo[$('#itemsList').val()-1]['description']);
					$('#type').val(itemInfo[$('#itemsList').val()-1]['type']);
				});
			</script>

	<?= $this->Form->create($item, ['type' => 'file']) ?>
<div class="manualInputPanelDiv">
    <div class="manualInputPanel">
    <h1>Manual Input</h1>
    <fieldset>
        <?php
            echo $this->Form->input('barcode',array('id'=>'barcode'));
            echo $this->Form->input('name', array('id'=>'name'));
            echo $this->Form->input('description', array('id'=>'description'));
        $opts = ['Frozen' => 'Frozen',
                 'Food cupboard' => 'Food cupboard',
                 'Dairy' => 'Dairy',
                 'Meat/Fish' => 'Meat/Fish',
                 'Fruit/Veg' => 'Fruit/Veg',
        ];
            echo $this->Form->input('food_type', ['label' => 'Food type', 'type' => 'select', 'options' => $opts, 'id'=>'type']);
            $opts = ['Use by' => 'Use by', 'Best before' => 'Best before'];
            echo $this->Form->input('type', ['label' => 'Expiry type', 'type' => 'select', 'options' => $opts]);
            echo $this->Form->input('expires');
            echo $this->Form->input('photo', ['type' => 'file']);
            echo $this->Form->input('photo_dir', ['type' => 'hidden']);
        ?>
		
    </fieldset>
	
    <!-- Need to add inputted image -->
		<div id = "submitButtonDiv">
			<?= $this->Form->button(__('Submit'), ['id' => 'submitButton'] ) ?>
			<?= $this->Form->end() ?>
		</div>

    </div>
  </div>
</div>
<!-- <div style="height: 1vh; background-color: black;"></div> -->
<div class="items">
  <h2 class="currentItems">Your Current Items</h2>
<?php  $count = 0;
        $today = strtotime(date("Y-m-d H:i:s"));
       foreach ($items as $item):

         $expires = strtotime($item["expires"]);
         $claimed = $item["claimed"];
         if($today<$expires and !$claimed):
           $count = $count+1;

?>
  <div class="itemBox">
    <?php if ($item->photo_dir): ?>
      <div class="imageDiv">
      <img src="<?= '/files/Items/photo/' . $item->photo ?>"></img>
      </div>
    <?php endif ?>
    <div class="infoDiv">
      <h1>Name: <?= h($item->name) ?></h1>
      <h1>Description: <?= h($item->description) ?></h1>
      <h1>Expiry Date: <?= h($item->expires) ?></h1>
      <h1>Food Type: <?= h($item->food_type) ?></h1>
      <h1><?= $this->Html->link(__('More Info...'), ['action' => 'view', $item->id]) ?></h1>
    </div>
</div>
    <?php endif ?>

<?php
endforeach
?>
<?php if($count==0): ?>
  <h1 class="emptyItems"> - No Current Items</h1>
  <?php endif ?>
</div>

<div class="expiredItems">
  <h2>Expired Items</h2>

<!-- Expired Items boxes -->

<?php  $count = 0;
        $today = strtotime(date("Y-m-d H:i:s"));
       foreach ($items as $item):
         $expires = strtotime($item["expires"]);
         $claimed = $item["claimed"];
         if($today>$expires and !$claimed):
           $count = $count+1;

?>
  <div class="expiredItemBox">
    <?php if ($item->photo_dir): ?>
      <div class="imageDiv">
      <img src="<?= '/files/Items/photo/' . $item->photo ?>"></img>
      </div>
    <?php endif ?>
    <div class="infoDiv">
      <h1>Name: <?= h($item->name) ?></h1>
      <h1>Description: <?= h($item->description) ?></h1>
      <h1>Expiry Date: <?= h($item->expires) ?></h1>
      <h1>Food Type: <?= h($item->food_type) ?></h1>
      <h1><?= $this->Html->link(__('More Info...'), ['action' => 'view', $item->id]) ?></h1>
    </div>
  </div>

    <?php endif ?>
<?php
endforeach
?>
<?php if($count==0): ?>
  <h1 class="emptyItems"> - No Expired Items</h1>
  <?php endif ?>
</div>
<!-- End of Expired item boxes   -->


<!-- Claimed item boxes -->
<div class="claimedItems">
  <h2>Claimed Items</h2>
<?php  $count = 0;
        $today = strtotime(date("Y-m-d H:i:s"));
       foreach ($items as $item):
         $expires = strtotime($item["expires"]);
         $claimed = $item["claimed"];
         if($claimed):
           $count = $count+1;

?>
  <div class="claimedItemBox">
    <?php if ($item->photo_dir): ?>
      <div class="imageDiv">
      <img src="<?= '/files/Items/photo/' . $item->photo ?>"></img>
      </div>
    <?php endif ?>
    <div class="infoDiv">
      <h1>Name: <?= h($item->name) ?></h1>
      <h1>Description: <?= h($item->description) ?></h1>
      <h1>Expiry Date: <?= h($item->expires) ?></h1>
      <h1>Food Type: <?= h($item->food_type) ?></h1>
      <h1><?= $this->Html->link(__('More Info...'), ['action' => 'view', $item->id]) ?></h1>
    </div>

      </div>
    <?php endif ?>
<?php
endforeach
?>
<?php if($count==0): ?>
  <h1 class="emptyItems"> - No Claimed Items</h1>

  <?php endif ?>
  </div>
<!-- End of claimed item boxes   -->

</div>
  <!-- Bottom nav-bar -->
  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	<div class="navButton">
		<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	<div class="title">
		<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	</div>
	<div class="navButton">
		<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	</div>
  </nav>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
	$.ajaxSetup({ cache: false });
</script>
<script src="/js/serratus-quaggaJS-b88a7b7/dist/quagga.js"></script>
<script>
//Hide and show barcode stuff
jQuery('div.barcodeStuff').hide();
jQuery(document).ready(function(){
        jQuery('.gotTheBarcode').on('click', function(event) {
             jQuery('div.barcodeStuff').slideToggle('fast');

        });
});
//end of 
</script>
<script>
jQuery('div.greyGivePanel').hide();
jQuery(document).ready(function(){
        jQuery('.toggleAddButton').on('click', function(event) {
             jQuery('div.greyGivePanel').slideToggle('fast');

        });
        jQuery('#allButton').on('click', function(event) {
             jQuery('div.items').show('fast');
             jQuery('div.expiredItems').show('fast');
             jQuery('div.claimedItems').show('fast');
        });
        jQuery('#currentButton').on('click', function(event) {
             jQuery('div.items').show('fast');
             jQuery('div.expiredItems').hide('fast');
             jQuery('div.claimedItems').hide('fast');
        });
        jQuery('#expiredButton').on('click', function(event) {
             jQuery('div.items').hide('fast');
             jQuery('div.expiredItems').show('fast');
             jQuery('div.claimedItems').hide('fast');
        });
        jQuery('#claimedButton').on('click', function(event) {
             jQuery('div.items').hide('fast');
             jQuery('div.expiredItems').hide('fast');
             jQuery('div.claimedItems').show('fast');
        });
    });
	

	

var trueBarcode = null;
var foundBarcode = false;
var barcodeCounter =  0;
var barcodes = [];
var liveInput;

	function readBarcodeLive()
	{
	//initialisation of the reader API - can be found at https://serratus.github.io/quaggaJS/#api
	liveInput = true;
	trueBarcode = null;
	foundBarcode = false;
	barcodes = [];

	document.getElementById("description").value = "";
	document.getElementById("name").value = "";
	document.getElementById("barcode").value = "";
			Quagga.init(
			{
				inputStream :
				{
					name : "Live",
					type : "LiveStream",
					target: document.querySelector('.cameraPane'),   // Or '#yourElement' (optional)
					frequency: 20
				},
				decoder :
				{
				//note more barcodes can go here but not entirely necessary - these are the only retail codes
					readers : ["upc_reader","ean_reader","ean_8_reader"]
				}
			},
			function(err)
			{
				if (err)
				{
				console.log(err);
				return
				}

				console.log("Initialization finished. Ready to start");
				Quagga.start();

			});

				//find barcode
				Quagga.onDetected(function callback(data)
				{
					if(liveInput == true)
					{
					console.log("In");
					barcodeCounter +=1;
					//incase it cant read the barcode
					if(barcodeCounter >= 50)
					{
						bestBarcode = null;
						bestNumber = 0;
						for(i = 0;i<barcodes.length;i++)
						{
							if(barcodes[i][1] > bestNumber && barcodes[i][0].length == 13)
							{
								bestNumber = barcodes[i][1];
								bestBarcode = barcodes[i][0]
							}
						}



						//document.getElementById("scanButtonLive").style.visibility = "hidden";
						document.getElementById("camera").style.visibility = "hidden";
						$(".drawingBuffer").hide();
						$("video").hide();
						$(".cameraPane").hide();
						Quagga.stop();
						alert("Couldn't determine exact barcode, found best fit");
						findItem(bestBarcode);
					}
					//console.log("yes");
					var newBarcode = true;

					//every time a barcode is detected, we check if its been registered before, and count up to 3 times (to avoid an error)
					for(i=0;i<barcodes.length;i++)
					{
						if(barcodes[i][0] != null &&barcodes[i][0] != "" && barcodes[i][0] != "undefined" && barcodes[i][0] == data.codeResult['code'])
						{
							newBarcode = false;
							barcodes[i][1] += 1;

							if(barcodes[i][1] >= 10)
							{
								foundBarcode = true;
								trueBarcode = data.codeResult['code'];
								break;
							}
						}
					}
					if(newBarcode)
					{
						barcodes.push([data.codeResult['code'],1]);
					}





					//ensure that it was able to register the barcode -
					if(foundBarcode)
					{
						//document.getElementById("scanButtonLive").style.visibility = "hidden";
						document.getElementById("camera").style.visibility = "hidden";
						$(".drawingBuffer").hide();
						$("video").hide();
						$(".cameraPane").hide();
						//continue with found barcode and close camera application
						Quagga.stop();
						findItem(trueBarcode);

					}
					}
					//styling issues
	//

				});


		//Quagga.start();


	}

	function findItem(barcode)
	{

		document.getElementById("barcode").value = barcode;
		console.log("barcode: " + barcode+ ".");

	//	var getReqLocation = "http://api.upcdatabase.org/json/efe8af7e06ee48bba4b0599e3e36112f/" + barcode;
		var getReqLocation =  "https://api.outpan.com/v2/products/" + barcode + "?apikey=1232f55222a95d636964993aecb6e2cd";
		console.log(getReqLocation);
		//sends a get request to the api
		$.get(getReqLocation, function(data,status)
		{
			console.log(data);
			//checks that the data has been found
			if(data.name == null)
			{
				alert("Sorry! Product not found in database, check the barcode is correct or input manually.");
				document.getElementById("description").value = "";
				document.getElementById("name").value = "";
				//document.getElementById("barcode").value = "";
			}
			else
			{
			//fill in the fields based on barcode info
				document.getElementById("name").value = data.name;
				console.log("this: " + data.attributes.Brand);
				var tags = "";
				for(var v in data.attributes)
				{
					tags += (data.attributes[v] +", ");
				}
				if(tags.length >=2)
				{
					tags = tags.substring(0,tags.length-2);
				}
				document.getElementById("description").value = tags;
			}
		});

	}

//from their example https://serratus.github.io/quaggaJS/examples
$(function() {
    var App = {
        init: function() {
            App.attachListeners();
        },
        config: {
            reader: "ean_reader",
            length: 10
        },
        attachListeners: function() {
            var self = this;

            $("#scanButtonPicture").on("click", function(e) {
			console.log("scan");
                var input = document.querySelector("#barcodePicture");
                if (input.files && input.files.length) {
                    App.decode(URL.createObjectURL(input.files[0]));
                }
            });

        },
        _accessByPath: function(obj, path, val) {
            var parts = path.split('.'),
                depth = parts.length,
                setter = (typeof val !== "undefined") ? true : false;

            return parts.reduce(function(o, key, i) {
                if (setter && (i + 1) === depth) {
                    o[key] = val;
                }
                return key in o ? o[key] : {};
            }, obj);
        },
        _convertNameToState: function(name) {
            return name.replace("_", ".").split("-").reduce(function(result, value) {
                return result + value.charAt(0).toUpperCase() + value.substring(1);
            });
        },
        decode: function(src) {
            var self = this,
                config = $.extend({}, self.state, {src: src});

            Quagga.decodeSingle(config, function(result) {});

        },
        setState: function(path, value) {
            var self = this;

            if (typeof self._accessByPath(self.inputMapper, path) === "function") {
                value = self._accessByPath(self.inputMapper, path)(value);
            }

            self._accessByPath(self.state, path, value);

            console.log(JSON.stringify(self.state));
            App.detachListeners();
            App.init();
        },
        inputMapper: {
            inputStream: {
                size: function(value){
                    return parseInt(value);
                }
            },
            numOfWorkers: function(value) {
                return parseInt(value);
            },
            decoder: {
                readers: function(value) {
                    if (value === 'ean_extended') {
                        return [{
                            format: "ean_reader",
                            config: {
                                supplements: [
                                    'ean_5_reader', 'ean_2_reader'
                                ]
                            }
                        }];
                    }
                    return [{
                        format: value + "_reader",
                        config: {}
                    }];
                }
            }
        },
        state: {
            inputStream: {
                size: 800
            },
            locator: {
                patchSize: "medium",
                halfSample: false
            },
            numOfWorkers: 1,
			decoder :
				{
				//note more barcodes can go here but not entirely necessary - these are the only retail codes
					readers : ["upc_reader","ean_reader","ean_8_reader"]
            },
            locate: true,
            src: null
        },
		error: function(e) {
			console.dir(e);
			}
    };

    App.init();

    Quagga.onDetected(function(result) {
	if(liveInput == false)
	{
        trueBarcode = result.codeResult['code'];
		if(trueBarcode == "undefined" || trueBarcode == null)
		{
			alert("Could not find barcode.");
		}
		else
		{
		findItem(trueBarcode);
		}
		}
    });
});
//switch for the 2 onDetected functions
function setLiveFalse()
{
	liveInput = false;
	trueBarcode = null;
	foundBarcode = false;
	barcodes = [];
	//reset the input fields
	document.getElementById("description").value = "";
	document.getElementById("name").value = "";
	document.getElementById("barcode").value = "";
}

function frequentItems()
{


}


</script>
