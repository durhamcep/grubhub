<?php
/**
  * @var \App\View\AppView $this
  */
?>
<head>
  <?= $this->Html->css('LoggedInTemplate.css') ?>
  <?= $this->Html->css('itemView.css') ?>
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>
<body>
	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

  <div style="height: 10vh; background-color: #9cd944;"></div>
<div style="min-height: 100vh;">
  <div class="findHeading">
  <div class="button" id="backButton">

  </div>
  <h1><?= h($item->name)?></h1>
  <div class="button" id="editButton">
    <?php
      if (!$item->reserved && $item->user_id != $user_id) {
        echo $this->Form->create(null, ['url' => ['action' => 'reserve', $item->id], 'method' => 'POST']);
        echo $this->Form->button('Reserve', array('class'=>"reserveButton"));
        echo $this->Form->end();
      }

      if ($item->user_id == $user_id) {
        echo $this->Html->link($this->Form->button('Edit Item'), array('action' => 'edit', $item->id), array('escape'=>false, 'title'=>"edit item", 'class'=>'btn toggleEditItem'));
      }
    ?>
  </div>

  </div>
  <div class="space"></div>
  <div class="greyDiv">
  <div class="pictureAndInfo">

    <?php if ($item->photo_dir): ?>
      <img src="<?= '/files/Items/photo/' . $item->photo ?>"></img>
    <?php endif ?>

  <div class="itemsInfo">
        <h1>Name: <?= h($item->name) ?></h1>
        <h1>Description: <?= h($item->description) ?></h1>
        <h1>Expiry Date: <?= h($item->expires) ?></h1>
        <h1>Type: <?= h($item->food_type) ?></h1>
        <h1>Created: <?= h($item->created) ?></h1>
        <h1>Reserved: <?= $item->reserved ? __('Yes') : __('No'); ?></h1>
		<h1>User: <a href = "../../users/view/ <?=($item->user->id) ?>"> <?= h($item->user->username) ?> </a> </h1>
    <?php if ($item->user_id == $auser['id']): ?>
        <a style="float: right" class="twitter-share-button"
        href="https://twitter.com/intent/tweet?text=I posted <?= $item->name ?> on grubhub.tk">
            Tweet</a>
    <?php else: ?>
        <a style="float: right" class="twitter-share-button"
        href="https://twitter.com/intent/tweet?text=Check out <?= $item->name ?> on grubhub.tk">
            Tweet</a>
    <?php endif ?>
  </div>
  </div>
</div>
</div>
<!-- Removed previous items description - King   -->

  <!-- Bottom nav-bar -->
  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	<div class="navButton">
		<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	<div class="title">
		<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	</div>
	<div class="navButton">
		<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	</div>
  </nav>

</body>

<script>window.twttr = (function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0],
         t = window.twttr || {};
     if (d.getElementById(id)) return t;
     js = d.createElement(s);
     js.id = id;
     js.src = "https://platform.twitter.com/widgets.js";
     fjs.parentNode.insertBefore(js, fjs);

     t._e = [];
     t.ready = function(f) {
         t._e.push(f);
     };

     return t;
 }(document, "script", "twitter-wjs"));</script>
