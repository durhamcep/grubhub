
<?php
/**
  * @var \App\View\AppView $this
  */
?>
<head>
  <?= $this->Html->css('LoggedInTemplate.css') ?>
  <?= $this->Html->css('itemSearch.css') ?>
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>
	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

<div style="height: 10vh; background-color: #9cd944;"></div>

<div class="itemList">

	<div class="findHeading">
		<!-- pushes right -->
		<div class="dropdown">
		<button class="btn btn-default" type="button" id="toggleFilters">
		  Filters
		</button>
		</div>

		<h1>Find</h1>
		<button class="btn btn-default" type="button" id="toggleSearchButton">
		  Toggle View
		</button>
	</div>

	<div class="sortAndFilterBy">
		<?php
			$sort=["Sort By", "Expiry (asc)","Expiry (desc)", "Distance (asc)", "Distance (desc)", "Name (asc)", "Name (desc)"];
			echo $this->Form->select('', $sort, array('options'=>$sort, 'id'=>'sortList', 'class'=>'btn btn-default dropdown-toggle'));
		?>
		<?php
			echo $this->Form->text('',array('placeholder'=>'value', 'type'=>'text','id'=>'filterVar', 'class'=>''));
			?>
		<?php
			$filter=["Filter By", "Expires Before (MM/DD/YY)","Expires After (MM/DD/YY)", "Distance (km)"]; 
			echo $this->Form->select('',$filter, array('type'=>'select','options'=>$filter, 'id'=>'filterList', 'class'=>'btn btn-default dropdown-toggle'));
		?>
		
		<div class="enterFilterButton">
				<?php
			echo $this->Form->submit('Apply filter', array('id'=>'filterButton', 'class'=>'btn btn-default', 'type'=>'button'));
			?>
		</div>
	</div>
	

	<div class="searchDiv">
	

	
	
	<?php echo $this->Form->create();
		  echo $this->Form->input('searchTerm', array('placeholder'=>'Search for an item', 'label'=>false));
		  echo $this->Form->button(__('Search'));
		  echo $this->Form->end();
	?>
	</div>
<!--- This is the Google Maps Stuff
#####################################################################################################################################
#####################################################################################################################################
-->

<script type="text/javascript" src='//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script type="text/javascript" src='//maps.google.com/maps/api/js?key=AIzaSyBgW5rxrwvIzKMWwWPPWfL3XJlyyLqAzdk&sensor=false'></script>
<script type="text/javascript" src='https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js'></script>
<script type="text/javascript">
      function initialize() {
        var center = new google.maps.LatLng(<?= $lat?>,<?=$long?>);
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 14,
          center: center,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var markers = [];
		function listener(map,marker,infowindow)
		{
			infowindow.open(map, marker);
		}
		<?php foreach ($items as $item): ?>

			var latLng = new google.maps.LatLng(<?=$item->user->lat?>,<?=$item->user->lon?>);
			var marker = new google.maps.Marker(
			{
				position: latLng, windowShow: true, windowContent: "<?=$item->name?>", title: "<?=$item->name?>", icon: 'http://labs.google.com/ridefinder/images/mm_20_purple.png'
			});
			var contentString='<div id="content"><h4>'+"<?=$item->name?>"+'</h4><br><p>'+"<?=$item->description?>"+'<br><a href="items/view/'+"<?=$item->id?>"+'" >View Item</a></p></div>';
			var infowindow = new google.maps.InfoWindow(
			{
			  content: contentString
			});
			marker.addListener('click', listener.bind(null,map,marker,infowindow) );
			markers.push(marker);
		<?php endforeach;?>

        var markerCluster = new MarkerClusterer(map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style type="text/css">

      #map_canvas {
        width: 80vw;
        height: 60vw;
		position: relative;
		margin: auto;
      }
</style>
<div id="map-container"><div id="map_canvas" class="mapDiv"></div></div>



<!--
#####################################################################################################################################
#####################################################################################################################################
-->

<div class="items index large-9 medium-8 columns content" style="width: 100vw;">

    <table cellpadding="0" cellspacing="0">
        <thead>


        </thead>
        <tbody>

		<!-- displays items -->
		<div id="itemBoxLoc">
            <?php foreach ($items as $item): ?>
              <div class="itemBox">
                  <h1>Name: <?= h($item->name) ?></h1>
                  <h1>Description: <?= h($item->description) ?></h1>
                  <h1>Expiry Date: <?= h($item->expires) ?></h1>
				  <h1>Distance: <?= ($item->distace=round(pow(pow(($lat-$item->user->lat)*69.1, 2)+pow(($long-$item->user->lon)*53.0, 2), 0.5),1))?></h1>
                  <h1><?= $this->Html->link(__('View More'), ['action' => 'view', $item->id]) ?></h1>


              </div>

            <?php endforeach; ?>
		</div>


        </tbody>
    </table>




    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>
<!-- <div style="height:100vh;"></div> -->
<div style="float: right">

  <!-- Bottom nav-bar -->
  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	<div class="navButton">
		<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	<div class="title">
		<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	</div>
	<div class="navButton">
		<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	</div>
  </nav>
</div>

</body>

<!-- JavaScript -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>
	<script>
		var allItems = [];
		var finalItemList = [];
		var changed = false;
		<?php foreach ($items as $i): ?>
			var item = {};
			var i = 0;
			item.name = "<?= $i->name; ?>"
			<?php $i->distance=round(pow(pow(($lat-$i->user->lat)*69.1, 2)+pow(($long-$i->user->lon)*53.0, 2), 0.5),1);?>
			item.distance = "<?= $i->distance; ?>"
			item.expires = "<?= $i->expires; ?>"
			item.description ="<?= $i->description; ?>"
			item.id="<?= $i->id; ?>"
			allItems.push(item);
		<?php endforeach; ?>

		$('#filterButton').click(function sortchoice()
		{
			var filterReq = $('#filterList').val();
			var filterValue = $('#filterVar').val();
			console.log(filterReq);
			if(filterReq == 1) //Expires Before
			{
				for(i = 0; i<allItems.length;i++)
				{
					if(allItems[i].expires.localeCompare(filterValue) == -1)
					{
						finalItemList.push(allItems[i]);
					}
				}
				changed = true;
			}
			else if(filterReq == 2) //Expires After
			{
				for(i = 0; i<allItems.length;i++)
				{
					if(allItems[i].expires.localeCompare(filterValue) == 1)
					{
						finalItemList.push(allItems[i]);
					}
				}
				changed = true;
			}
			else if(filterReq == 3) //within distance
			{
				for(i = 0; i<allItems.length;i++)
				{
					if(allItems[i].distance <= Number(filterValue))
					{
						finalItemList.push(allItems[i]);
					}
				}
				changed = true;
			}
			else if(filterReq == 0)
			{
				for(i = 0; i<allItems.length;i++)
				{
					finalItemList.push(allItems[i]);
				}
				changed = true;
			}

			if(changed)
			{
				console.log("yas");
				$(".itemBox").remove();
				showBoxesFilter();
				changed = false;
			}
		});

		function showBoxesFilter()
		{
		for(i = 0;i<finalItemList.length;i++)
		{
			var itemBox = document.createElement('div');
			itemBox.className ='itemBox';
			itemBox.innerHTML="<h1>Name: " +finalItemList[i].name+" </h1> <h1>Description: "+finalItemList[i].description+"</h1> <h1>Expiry Date: "+finalItemList[i].expires+"</h1> <h1>Distance: "+finalItemList[i].distance+"</h1><h1><a href='items/view/"+finalItemList[i].id+"'>View More</a></h1>";
			document.getElementById("itemBoxLoc").appendChild(itemBox);
		}
		finalItemList = [];
	}
</script>

<script>

	var allItems = [];
	<?php foreach ($items as $i): ?>
			var item = {};
			var i = 0;
			item.name = "<?= $i->name; ?>"

			<?php $i->distance=round(pow(pow(($lat-$i->user->lat)*69.1, 2)+pow(($long-$i->user->lon)*53.0, 2), 0.5),1);?>
			item.distance = Number(<?= $i->distance; ?>)
			item.expires = "<?= $i->expires; ?>"
			item.description ="<?= $i->description; ?>"
			item.id="<?= $i->id; ?>"
			allItems.push(item);
	<?php endforeach; ?>
	$('#sortList').click(function sortchoice()
	{


		var changed = false;
		var sortValue = $('#sortList').val();
		if(sortValue == 1) //expiry asc
		{
			allItems.sort(compareExp);
			changed = true;
		}
		else if(sortValue == 2) //expiry desc
		{
			allItems.sort(compareExpD);
			changed = true;
		}
		else if(sortValue == 3) //Distance (asc)
		{
			allItems.sort(compareDst);
			changed = true;
		}
		else if(sortValue == 4) //Distance (desc)
		{
			allItems.sort(compareDstD);
			changed = true;
		}
			else if(sortValue == 5) //Name (desc)
		{
			allItems.sort(compareName);
			changed = true;
		}
			else if(sortValue == 6) //Name (desc)
		{
			allItems.sort(compareNameD);
			changed = true;
		}
		if(changed)
		{
			$(".itemBox").remove();

			showBoxes();
			changed = false;
		}

	});

function showBoxes()
{
	for(i = 0;i<allItems.length;i++)
	{
		var itemBox = document.createElement('div');
		itemBox.className ='itemBox';
		itemBox.innerHTML="<h1>Name: " +allItems[i].name+" </h1> <h1>Description: "+allItems[i].description+"</h1> <h1>Expiry Date: "+allItems[i].expires+"</h1> <h1>Distance: "+allItems[i].distance+"</h1><h1><a href='items/view/"+allItems[i].id+"'>View More</a></h1>";
		document.getElementById("itemBoxLoc").appendChild(itemBox);
	}
}

function compareDst(a,b)
{

	if (a.distance < b.distance)
	{
		return -1;
	}

	if (a.distance > b.distance)
	{
		return 1;
	}

	return 0;
}
function compareName(a,b)
{
	if (a.name.localeCompare(b.name) == -1)
	{
		return -1;
	}

	if (a.name.localeCompare(b.name) == 1)
	{
		return 1;
	}

	return 0;
}
function compareExp(a,b)
{

	if (a.expires.localeCompare(b.expires) == -1)
	{
		return -1;
	}
	if (a.expires.localeCompare(b.expires) == 1)
	{
		return 1;
	}
	return 0;
}

function compareDstD(a,b)
{
	if (a.distance > b.distance)
	{
		return -1;
	}

	if (a.distance < b.distance)
	{
		return 1;
	}

	return 0;
}
function compareNameD(a,b)
{
	if (a.name.localeCompare(b.name) == 1)
	{
		return -1;
	}

	if (a.name.localeCompare(b.name) == -1)
	{
		return 1;
	}

	return 0;
}
function compareExpD(a,b)
{

	if (a.expires.localeCompare(b.expires) == 1)
	{
		return -1;
	}
	if (a.expires.localeCompare(b.expires) == -1)
	{
		return 1;
	}
	return 0;
}


</script>
<script>
jQuery('div.items').toggle('fast');
jQuery('button#dropdownMenu1').toggle('fast');
jQuery(document).ready(function(){
        jQuery('#toggleSearchButton').on('click', function(event) {
             jQuery('div.mapDiv').slideToggle('fast');
             jQuery('div.items').slideToggle('fast');
             jQuery('button#dropdownMenu1').slideToggle('fast');


        });
    });
	
jQuery('div.sortAndFilterBy').toggle('fast');
jQuery(document).ready(function(){
        jQuery('#toggleFilters').on('click', function(event) {
             jQuery('div.sortAndFilterBy').slideToggle('fast');

        });
    });
</script>
