<?php
/**
  * @var \App\View\AppView $this
  */
?>

<head>
    <!-- bootstrap framework -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> breaks things-->
    <?= $this->Html->css('mainTemplate.css') ?>
    <?= $this->Html->css('CreateUser.css') ?>
	<?= $this->Html->script('/js/passwordAnalysis')?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<!-- text font cross platform-->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
</head>

<body>
	<!-- navigation bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
	</nav>

	<div class="add-createUser">
		<?= $this->Form->create($user, ['type' => 'file']) ?>
		<fieldset>
			<legend><?= __('Create an account') ?></legend>
			<?php
				echo $this->Form->input('username', array('placeholder'=>'username', 'label'=>false));
				echo $this->Form->input('email', array('placeholder'=>'email', 'label'=>false));

				echo $this->Form->input('password', array('placeholder'=>'password', 'label'=>false));
				echo $this->Form->input('passwordAnalysis',array('id'=>'passwordAnalysis','label' => false, 'readonly' => true));

				echo $this->Form->input('postcode', array('placeholder'=>'postcode', 'label'=>false));
				echo $this->Form->input('photo', ['type' => 'file'], array('placeholder'=>'photo', 'label'=>false));
				echo $this->Form->input('photo_dir', ['type' => 'hidden'], array('placeholder'=>'photo_dir', 'label'=>false));
			?>
		</fieldset>

		<script>

			$('#password').on("keyup", function callBack()
			{
				var passwordTest=analysePassword($('#password').val());
				console.log(passwordTest);
				 $('#passwordAnalysis').val(passwordTest);
			});

			function analysePassword(password)
			{

				var passwordStrength = 0;
				var lengthGain = 0.3 * password.length;

				var noOfUpper = 0;
				var noOfLower = 0;
				var numbers = "0123456789";
				var containsNumbers = false;
				for(i = 0; i< password.length;i++)
				{

					if(password.charAt(i)== password.charAt(i).toUpperCase())
					{
						noOfUpper+=1;
					}
					else
					{
						noOfLower+=1;
					}
					if(numbers.indexOf(password.charAt(i))> -1)
					{
						containsNumbers = true;
					}

				}

				var AltChar = Math.floor(((noOfUpper) * (noOfLower))^0.2);
				if(containsNumbers)
				{
					AltChar = (AltChar*1.5 + 1);
				}

				passwordStrength = lengthGain + AltChar;

				if(passwordStrength <10)
				{
					return "Weak Password";
				}
				else if(passwordStrength <20)
				{
					return "Medium Password";
				}
				else if(passwordStrength <30)
				{
					return "Strong Password";
				}
				else
				{
					return "Very Strong Password";
				}
			}
		</script>

		<div class="button-container">
			<?= $this->Form->button(__('Create!'), array('class'=>'createUserButton')) ?>
			<?= $this->Form->end() ?>
		</div>


		<div class="add-info">
		<!-- white space after form -->
		</div>

	</div>

	<!-- Bottom nav-bar -->
	<nav class="navbar navbar-default">
		<div class ="bottom-nav">
			<div class = "loggedOutBottomNavLeft">
				<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
			</div>
			<div class = "loggedOutBottomNavRight">
				<?= $this->Html->link(__('Contact Us'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
			</div>
		</div>
	</nav>

</body>