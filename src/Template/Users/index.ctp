<?php
/**
  * @var \App\View\AppView $this
  */
?>

<head>
    <!-- bootstrap framework -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('LoggedInTemplate.css') ?>
    <?= $this->Html->css('userLoggedInHome.css') ?>

</head>

<body class="userLoggedInHome">
	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

  <div style="height: 10vh; background-color: #9cd944;"></div>


  <div class="notificationHeading">
	  <!-- hall of fame button -->
    <div style="width: 25vw; display: inline-block;">
		<?= $this->Html->link($this->Form->button('Hall of Fame'), array('action' => 'halloffame'), array('escape'=>false, 'title'=>"hall of fame", 'id'=>'halloffame'));?>
    </div>
		<!-- Notifications title -->
      <h1 style="width: 50vw;">Notifications</h1>
	  <!-- alerts button-->
    <div style="width: 25vw;display: inline-block;">
		<?= $this->Html->link($this->Form->button('Alerts'), array('action' => 'alerts'), array('escape'=>false, 'title'=>"alerts", 'class'=>'btn toggleAlerts'));?>
  </div>
  </div>



  <div class="notificationMain">
      <h2 class="notificationTitle">Unread</h2>
      <?php $count = 0; ?>
      <?php foreach ($unread_notifications as $notification): ?>
          <?php $count++; ?>
          <div class='unread-notification'>
          <?php if ($notification->ntype == 'message'): ?>
              <div class='message-notification'>
                  <?php
                  $chat = $notification->message->chat;
                  if ($chat->item_owner == $auser['id']) {
                      echo $chat->from->username . " says " . $notification->message->message;
                  } else {
                      echo $chat->to->username . " says " . $notification->message->message;
                  }
                  ?>
              </div>
          <?php elseif ($notification->ntype == 'claimed_item'): ?>
              <div class='claimed-item-notification'>
                  The owner of
                  <?= $this->Html->link($notification->item->name,
                                        ['controller' => 'items', 'action' => 'view', $notification->item->id]) ?>
                  has said you have picked up this item.
                  <?= $this->Html->link('You can now give them a rating (under your reserved items list)',
                                        ['controller' => 'users', 'action' => 'view', $auser['id']]) ?>
              </div>
          <?php elseif ($notification->ntype == 'item_alert'): ?>
              <div class='item-alert-notification'>
                  <?= $this->Html->link('An item you have set up an alert for has been posted.', ['controller' => 'items', 'action' => 'view', $notification->item->id]); ?>
              </div>
              <?php elseif ($notification->ntype == 'favourite_user_posted_item'): ?>
                  <div class='favourite-user-item-notification'>
                      A <?= $this->Html->link('user', ['action' => 'view', $notification->item->user_id]) ?> you favourited has posted <?= $this->Html->link($notification->item->name, ['controller' => 'items', 'action' => 'view', $notification->item->id]) ?>.
                  </div>
          <?php endif ?>
                  </div>
      <?php endforeach ?>
      <?php if ($count == 0): ?>
          <div class="noNotifications">No unread notifications</div>
      <?php endif ?>


      <h2 class="notificationTitle">Read</h2>
      <?php $count = 0; ?>
      <?php foreach ($read_notifications as $notification): ?>
          <?php $count++; ?>
          <div class='read-notification'>
          <?php if ($notification->ntype == 'message'): ?>
              <div class='message-notification'>
                  <?php
                  $chat = $notification->message->chat;
                  if ($chat->item_owner == $auser['id']) {
                      echo $chat->from->username . " says " . $notification->message->message;
                  } else {
                      echo $chat->to->username . " says " . $notification->message->message;
                  }
                  ?>
              </div>
          <?php elseif ($notification->ntype == 'claimed_item'): ?>
              <div class='claimed-item-notification'>
                  The owner of
                  <?= $this->Html->link($notification->item->name,
                                        ['controller' => 'items', 'action' => 'view', $notification->item->id]) ?>
                  has said you have picked up this item.
                  <?= $this->Html->link('You can now give them a rating (under your reserved items list)',
                                        ['controller' => 'users', 'action' => 'view', $auser['id']]) ?>
              </div>
          <?php elseif ($notification->ntype == 'item_alert'): ?>
              <div class='item-alert-notification'>
                  <?= $this->Html->link('An item you have set up an alert for has been posted.', ['controller' => 'items', 'action' => 'view', $notification->item->id]); ?>
              </div>
              <?php elseif ($notification->ntype == 'favourite_user_posted_item'): ?>
                  <div class='favourite-user-item-notification'>
                      A <?= $this->Html->link('user', ['action' => 'view', $notification->item->user_id]) ?> you favourited has posted <?= $this->Html->link($notification->item->name, ['controller' => 'items', 'action' => 'view', $notification->item->id]) ?>.
                  </div>
          <?php endif ?>
          </div>
      <?php endforeach ?>
      <?php if ($count == 0): ?>
          <div class="noNotifications">No read notifications</div>
      <?php endif ?>
  </div>

  <!-- Bottom nav-bar -->
  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	<div class="navButton">
		<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	<div class="title">
		<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	</div>
	<div class="navButton">
		<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	</div>
  </nav>
</body>
