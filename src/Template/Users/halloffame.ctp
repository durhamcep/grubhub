
<head>
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<?= $this->Html->css('LoggedInTemplate.css') ?>
	<?= $this->Html->css('hallOfFame.css')?>
</head>


<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
	<div class="title">
	<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
	</div>
</nav>


<div class="hofHeading">

		<h1>Hall Of Fame</h1>

</div>


<div class="hofBody" style="height: 100vh;">
<table>
	<tr>
		<th id="tableHeading"> Username </th>
		<th id="tableHeading"> Score </th>
	</tr>
	<?php foreach($halloffame as $row): ?>
		<tr>
			<td id="tableRow"> <?= $row['userName']?> </td>
			<td id ="tableRow"> <?= $this->Number->format($row['scr'], ['places' => 1]) ?> </td>
	<?php endforeach?>
	</table>
</div>



<nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
<div class="navButton">
	<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
	</div>
<div class="title">
	<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
</div>
<div class="navButton">
	<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
</div>
</nav>

