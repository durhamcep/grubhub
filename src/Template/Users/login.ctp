<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>

<head>
    <!-- bootstrap framework -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('mainTemplate.css') ?>
    <?= $this->Html->css('home.css') ?>

	<!-- text font cross platform-->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
</head>

<body class="home">
	<!-- navigation bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="title">
		<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
	</nav>

	<div style="height: 10vh; background-color: #9cd944;"></div>

	<!-- login area -->
	<div class="home-login">
		<?= $this->Flash->render('auth') ?>
		<?= $this->Form->create() ?>
		<fieldset>
			<?php echo $this->Form->input('username', array('placeholder'=>'username', 'label'=>false)); ?>
			<?php echo $this->Form->input('password', array('label'=>false, 'placeholder'=>"password")); ?>
		</fieldset>
		<form style="text-align: center">
		<?= $this->Form->button(__('Login'), array('class'=>'loginSubmitButton')); ?>
		<?= $this->Form->end() ?>

	<!-- Extra login details -->
		<a href="/users/reset"><h1 class="forgottenPasswordLink">Forgotten your password?</h1></a>
		<div class="orWithLines">
			<div class="line"></div>
				<h1 class="or">or</h1>
			<div class="line"></div>
		</div>
			<a href="#"><?= $this->Html->link(__('Sign up here'), ['controller'=>'users','action' => 'add'], ['class'=>"signUpLink"]) ?> </a>
		</div>
	</div>

	<!-- Information area 1 -->
	<div class="home-info-grey"style="background-color: rgb(216,216,216); height: 90vh">
		<div class="leftSect">
			  <h1 id="giveTitle">Give</h1>
			  <h2>Find people in your local area to put your unwanted groceries to better use.</h2>
		</div>
		<div class="rightSect">
			  <h1 id="shareTitle">Share</h1>
			  <h2>Give and receive items easily with the simple to use search feature</h2>
		</div>
	</div>

	<!-- Information area 2 -->
	<div class="home-info-white" style="background-color: #FFFFFF; height:100vh">
	  <div class="leftSect">
			<h1 id="giveTitle">Show</h1>
			<h2>Show your friends your achievements through social media</h2>
	  </div>
	  <div class="rightSect">
			<h1 id="shareTitle">Earn</h1>
			<h2>Earn points by using GrubHub and climb up the hall of fame</h2>
	  </div>
	</div>

	<!-- Bottom navigation bar -->
	<nav class="navbar navbar-default">
		<div class ="bottom-nav">
			<div class = "loggedOutBottomNavLeft">
				<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
			</div>
			<div class = "loggedOutBottomNavRight">
				<?= $this->Html->link(__('Contact Us'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
			</div>
		</div>
	</nav>


</body>

