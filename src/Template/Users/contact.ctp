<?php
/**
  * @var \App\View\AppView $this
  */
?>

<head>
    <!-- bootstrap framework -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('loggedInTemplate.css') ?>
    <?= $this->Html->css('userContactUs.css') ?>

</head>

<body class="userLoggedInHome">
  <nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
    <div class="title">
    <?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
    </div>
  </nav>

 <div style="height: 10vh; background-color: #9cd944;"></div>

<h1>Got a question?</h1>
<h2>Contact us at cs-seg5@durham.ac.uk</h2>

  <div class="whatIsGH"></div>


  <!-- Bottom nav-bar -->
  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
	<div class="navButton">
		<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
    </div>
	<div class="title">

	</div>
	<div class="navButton">
		<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	</div>
  </nav>
</body>