<?php
/**
  * @var \App\View\AppView $this
  */
  use Cake\Datasource\ConnectionManager;
?>



<head>
    <!-- bootstrap framework -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('LoggedInTemplate.css') ?>
    <?= $this->Html->css('userProfile.css') ?>
</head>

<body class="userProfile">

	<!-- Top nav-bar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
		<div class="navButton">
			<?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
		</div>
	</nav>

	<div style="height: 10vh; background-color: #9cd944;"></div>
	<?php if ($auth_user['id'] != $user->id)
	{

		$user1 = $auth_user['id'];
		$favouritedUser = $user->id;

		$conn = ConnectionManager::get('default');
		$existsQuery = $conn->execute('SELECT COUNT(user)
					FROM `favourite_users`
					WHERE user = ? AND favourited_user = ?',[$user1,$favouritedUser])->fetch("assoc");

		$exists = $existsQuery["COUNT(user)"];
		$GLOBALS['exists'] = $exists;
	}
	else
	{
	  $GLOBALS['exists'] = 2;
	}?>



	<div class="profileHeading">
		<div style="width: 25vw;"></div>

		<h1>Profile</h1>

		<?php if ($auth_user['id'] == $user->id): ?>
			<?= $this->Html->link($this->Form->button('Edit User'), array('action' => 'edit', $user->id), array('escape'=>false, 'title'=>"edit user", 'class'=>'btn toggleEditUser'));?>

		<?php else: ?>

			<?php if($GLOBALS['exists'] == 0): ?>
			  <button type= "button" onclick = "favouriteUser()" id = "FavouriteButton" > Favourite! </button>
			  <button type= "button" onclick = "unfavouriteUser()" style="display:none;" id = "UnfavouriteButton" > Unfavourite! </button>
			<?php endif ?>

			<?php if($GLOBALS['exists'] == 1): ?>
			  <button type= "button" onclick = "favouriteUser()" style="display:none;" id = "FavouriteButton" > Favourite! </button>
			  <button type= "button" onclick = "unfavouriteUser()"  id = "UnfavouriteButton" > Unfavourite! </button>
			<?php endif ?>

		<?php endif ?>

	</div>


	<div class="profileInformation">
		<h1> </h1>
		<div><h2><?= h($user->username) ?></h2></div>

		<?php if ($user->photo_dir): ?>
			  <img class = "profilePicture" src="<?= '/files/Users/photo/' . $user->photo ?>"></img>
		<?php endif ?>


		<div class="user-details">
			<h2>Details</h2>

			<table class="vertical-table">
			  <?php if ($auth_user['id'] == $user->id): ?>
				<tr>
					<th scope="row"><?= __('Email') ?></th>
					<td><?= h($user->email) ?></td>
				</tr>
				<tr>
					<th scope="row"><?= __('Postcode') ?></th>
					<td><?= h($user->postcode) ?></td>
				</tr>
			  <?php endif ?>
				<tr>
					<th scope="row"><?= __('Username') ?></th>
					<td><?= h($user->username) ?></td>  <!-- This is getting $user row from the controller from the table and then the username from that -->
				</tr>
				<tr>
					<th scope="row"><?= __('Id') ?></th>
					<td><?= $this->Number->format($user->id) ?></td>
				</tr>
				<tr>
					<th scope="row"><?= __('Average score') ?></th>
					<td><?= $rating ?></td>
				</tr>
			</table>
		</div>

		<div class="posted-items">
			<h2>Posted items</h2>
			<ul>
				<?php foreach($user->owned_items as $item): ?>
					<li><?= $this->Html->link($item->name, ['controller' => 'items', 'action' => 'view', $item->id]) ?></li>
				<?php endforeach ?>
			</ul>
			<?php if ($auth_user['id'] == $user->id && count($items) != 0): ?>
				You seem to give away a lot of the following items:
				<ul>
					<?php foreach ($items as $item): ?>
						<li><?= $item['name'] ?> (<?= $item['count'] ?>)</li>
					<?php endforeach ?>
				</ul>
				Why not try buying less of them next time you go shopping, or perhaps
				look for some <a href="#" id="recipe-link">recipies</a> with these ingredients to use up the leftovers.
				<form id="recipe-form" action="http://www.bbc.co.uk/food/recipes/search" method="post">
					<input type="hidden" name="keywords" value="<?php foreach ($items as $item): ?><?= $item['name'] ?> <?php endforeach ?>" />
				</form>
			<?php endif ?>
		</div>


		<?php if ($auth_user['id'] == $user->id): ?>
			<div class="reserved-items">
				<h2>Reserved items</h2>

				<!-- reserved items table -->
				<table>
					<tr>
						<th>Item</th>
						<th>User</th>
						<th>Status</th>
						<th>Your Rating</th>
					</tr>
					<?php foreach($user->reserved_items as $item): ?>
						<tr>
							<td id = "item-<?= $item->id ?>"> <?= $this->Html->link($item->name, ['controller' => 'items', 'action' => 'view', $item->id]) ?></td>
							<td><?= $this->Html->link($item->user->username, ['controller' => 'Users', 'action' => 'view', $item->user->id]) ?></td>
							<td><?php if($item->claimed): ?>Claimed<?php else: ?>Reserved<?php endif ?></td>
							<?php if ($item->claimed): ?>
								<?php if ($item->rating): ?>
									<td><?= $item->rating->score ?></td>
									<?php else: ?>
										<td id = "dropdown-<?= $item->id ?>">
											<select id = "rating-<?= $item->id ?>">
												<option> </option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</td>
								<?php endif ?>
							<?php endif ?>
						</tr>
					<?php endforeach ?>

					<ul>
						<?php if (sizeof($user->reserved_items) == 0): ?>
							<h5>You do not have any items reserved</h5>
						<?php endif ?>
					</ul>
				</table>

				<input type = "submit" onclick = "updateTableAndScores()" value="Submit rating(s)"></input>
			</div>
		<?php endif ?>

		<div class = "achievements">
		<!-- setup for awards -->
			<?php
				$achi1 = false;
				$achi2 = false;
				$achi3 = false;
				foreach($GA as $rowGA): ?>
					<?php if($rowGA['achievementname']=="Add an item")
					{
						$achi1 = true;
					}
					elseif($rowGA['achievementname']=="Get a 5 star ratings")
					{
						$achi2 = true;
					}
					elseif($rowGA['achievementname']=="Add 10 items")
					{
						$achi3 = true;
					}
					?>
			<?php endforeach ?>

			<h2> Achievements</h2>
				<div class = "award1">
					<h4> Add an Item </h4>
					<?php if($achi1): ?>
						<img src="/img/give1item.png" style="height:100px;">
					<?php else: ?>
						<img src="/img/give1itemGRAY.png" style="height:100px;">
					<?php endif ?>
				</div>

				<div class = "award2">
					<h4> get a 5* rating </h4>
					<?php if($achi2): ?>
						<img src="/img/get5star.png" style="height:100px;">
					<?php else: ?>
						<img src="/img/get5starGRAY.png" style="height:100px;">
					<?php endif ?>
				</div>

				<div class = "award3">
					<h4> Give 10 items </h4>
					<?php if($achi3): ?>
						<img src="/img/give10items.png" style="height:100px;">
					<?php else: ?>
						<img src="/img/give10itemsGRAY.png" style="height:100px;">
					<?php endif ?>
				</div>

		</div>

		<div class="blockButton">
			<?php if ($auth_user['id'] != $user->id): ?>
				<?= $this->Html->link($this->Form->button('Block user', ["id"=>"blockButton"]), ['action' => 'block', $user->id], ['escape' => false, 'class' => 'btn']) ?>
			<?php endif ?>
		</div>

	<!-- Bottom nav-bar -->
	  <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
		<div class="navButton">
			<?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
		</div>
		<div class="title">
			<?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
		</div>
		<div class="navButton">
			<?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
		</div>
	  </nav>
	</nav>
</body>

<!-- JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script>
function favouriteUser()
{
		$.ajax({
				dataType: 'json',
				data: {'usr' : <?= $auth_user['id'] ?>, 'fav':  <?= $user->id ?>},
				type: 'POST',
				url: "/users/favourite",
				success: function(data) {
					//document.getElementById("FavouriteButton").style.visibility = "hidden";
					//document.getElementById("UnfavouriteButton").style.visibility = "visible";
					$('#FavouriteButton').hide();
					$('#UnfavouriteButton').show();
				},
				 error: function (err)
				{ alert(err.responseText)}
			});



}
function unfavouriteUser()
{
	$.ajax({
				dataType: 'json',
				data: {'usr' : <?= $auth_user['id'] ?>, 'fav':  <?= $user->id ?>},
				type: 'POST',
				url: "/users/unfavourite",
				success: function(data) {
				},
				 error: function (err)
				{ alert(err.responseText)}
			});

	//document.getElementById("FavouriteButton").style.visibility = "visible";
	//document.getElementById("UnfavouriteButton").style.visibility = "hidden";
	$('#UnfavouriteButton').hide();
	$('#FavouriteButton').show();
}
function updateTableAndScores()
{
	<?php foreach($user->reserved_items as $item): ?>
		var a = document.getElementById("rating-<?= $item->id ?>");
		if(a != null)
		{
		console.log(a.value);
		if(a.value != " ")
		{

			$.ajax({
				dataType: 'json',
				data: {'score': a.value},
				type: 'POST',
				url: "/items/rate/<?= $item->id ?>",
				success: function(data) {
					console.log("success");
					document.getElementById("dropdown-<?= $item->id ?>").innerHTML = ('<p>' + a.value + '</p>');
				},
				 error: function (err)
				{ alert(err.responseText)}
			});

		}
		}
	<?php endforeach ?>
}

 $("#recipe-link").click(function() {
     $("#recipe-form").submit();
 });
</script>

