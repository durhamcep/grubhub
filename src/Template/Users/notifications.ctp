<?php foreach ($notifications as $notification): ?>
  <?php if ($notification->read): ?>
    <div class='read-notification'>
  <?php else: ?>
    <div class='unread-notification'>
  <?php endif ?>
  <?php if ($notification->ntype == 'message'): ?>
    <div class='message-notification'>
      <?php
        $chat = $notification->message->chat;
        if ($chat->item_owner == $auser['id']) {
          echo $chat->from->username . " says " . $notification->message->message;
        } else {
            echo $chat->to->username . " says " . $notification->message->message;
        }
      ?>
      </div>
    <?php elseif ($notification->ntype == 'claimed_item'): ?>
      <div class='claimed-item-notification'>
           The owner of
           <?= $this->Html->link($notification->item->name,
               ['controller' => 'items', 'action' => 'view', $notification->item->id]) ?>
           has said you have picked up this item.
           <?= $this->Html->link('You can now give them a rating (under your reserved items list)',
               ['controller' => 'users', 'action' => 'view', $auser['id']]) ?>
      </div>
    <?php elseif ($notification->ntype == 'item_alert'): ?>
      <div class='item-alert-notification'>
        <?= $this->Html->link('An item you have set up an alert for has been posted.', ['controller' => 'items', 'action' => 'view', $notification->item->id]); ?>
      </div>
    <?php endif ?>
  </div>
<?php endforeach ?>