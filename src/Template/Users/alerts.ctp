<head>
    <!-- bootstrap framework -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?= $this->Html->css('LoggedInTemplate.css') ?>
    <?= $this->Html->css('alerts.css') ?>
</head>

<body class="userLoggedInHome">
    <!-- Top nav-bar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="width: 100vw;" id="navigBar">
        <div class="navButton">
            <?= $this->Html->link(__('Find'), ['controller'=>'items','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-find')) ?>
        </div>
        <div class="navButton">
            <?= $this->Html->link(__('Give'), ['controller'=>'items','action' => 'add'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-give')) ?>
        </div>
        <div class="title">
            <?= $this->Html->link(__('GrubHub'), ['controller'=>'users','action' => 'index'], array('id'=>'mainPageTitle')) ?>
        </div>
        <div class="navButton">
            <?= $this->Html->link(__('Profile'), ['controller'=>'users/view','action' => $auser["id"]], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-profile')) ?>
        </div>
        <div class="navButton">
            <?= $this->Html->link(__('Chat'), ['controller'=>'chats','action' => 'index'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-chat')) ?>
        </div>
    </nav>

    <div style="height: 10vh; background-color: #9cd944;"></div>
    <div class="alertHeading">
        <div style="width: 25vw"></div>
        <h1>Alerts</h1>
    </div>
    <div class="alertMain">
        <h2 class="alertTitle">Create an alert</h3>
            <div class="info">If you create an alert for an item then you will get a alert every time a user posts one of these items is posted with the number of miles you specify.</div>
        <?php
          echo $this->Form->create($a, ['method' => 'post']);
        echo $this->Form->control('item_name');
        echo $this->Form->control('distance', ['type' => 'number']);
        echo $this->Form->submit('Add');
        ?>

        <h2 class="alertTitle">Your current alerts</h3>
        <ul>
            <?php foreach($alerts as $alert): ?>
                <li><?= $alert->item_name ?> <?= $this->Html->link('Delete', ['action' => 'deletealert', $alert->id]) ?></li>
            <?php endforeach ?>
        </ul>
        </div>
        <!-- Bottom nav-bar -->
        <nav class="navbar navbar-default navbar-static-bottom" style = "position: relative; bottom:0">
            <div class="navButton">
                <?= $this->Html->link(__('Privacy   '), ['controller'=>'users','action' => 'privacy'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-privacy')) ?>
            </div>
            <div class="title">
                <?= $this->Html->link(__('Log Out'), ['controller'=>'users','action' => 'logout'], array('class'=>'navBarOptions', 'id'=> 'mainPageTitle')) ?>
	          </div>
	          <div class="navButton">
		            <?= $this->Html->link(__('Contact'), ['controller'=>'users','action' => 'contact'], array('class'=>'navBarOptions', 'id'=> 'navBarOptions-contact')) ?>
	          </div>
        </nav>

</body>
