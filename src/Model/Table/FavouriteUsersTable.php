<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FavouriteUsers Model
 *
 * @method \App\Model\Entity\FavouriteUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\FavouriteUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FavouriteUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FavouriteUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FavouriteUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FavouriteUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FavouriteUser findOrCreate($search, callable $callback = null, $options = [])
 */
class FavouriteUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('favourite_users');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('user')
            ->requirePresence('user', 'create')
            ->notEmpty('user');

        $validator
            ->integer('favourited_user')
            ->requirePresence('favourited_user', 'create')
            ->notEmpty('favourited_user');

        return $validator;
    }
}
