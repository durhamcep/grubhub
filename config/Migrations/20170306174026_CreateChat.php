<?php
use Migrations\AbstractMigration;

class CreateChat extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('chats');
        $table->addColumn('item_owner', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('item_owner', 'users', 'id');
        $table->addColumn('reserver', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('reserver', 'users', 'id');
        $table->addColumn('item_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('item_id', 'items', 'id');
        $table->addColumn('closed', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->create();
    }
}
