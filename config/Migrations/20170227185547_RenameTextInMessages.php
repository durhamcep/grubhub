<?php
use Migrations\AbstractMigration;

class RenameTextInMessages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('messages');
        $table->renameColumn('text', 'message');
        $table->renameColumn('read', 'been_read');
        $table->renameColumn('to', 'mto');
        $table->renameColumn('from', 'mfrom');
    }
}
