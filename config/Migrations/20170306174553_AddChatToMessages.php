<?php
use Migrations\AbstractMigration;

class AddChatToMessages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('messages');
        $table->addColumn('chat_id', 'integer', [
            'length' => 11,
            'default' => null,
            'null' => false,
        ]);
        $table->addForeignKey('chat_id', 'chats', 'id');
        $table->update();
    }
}
