<?php
use Migrations\AbstractMigration;

class CreateBlocks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('blocks');
        $table->addColumn('blocker', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('blocker', 'users', 'id');
        $table->addColumn('blockee', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('blockee', 'users', 'id');
        $table->create();
    }
}
