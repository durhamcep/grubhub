<?php
use Migrations\AbstractMigration;

class CreateFavouriteUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('favourite_users');
		$table->addColumn('user', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('user', 'users', 'id');
		
		$table->addColumn('favourited_user', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('favourited_user', 'users', 'id');
		
		
        $table->create();
    }
}
