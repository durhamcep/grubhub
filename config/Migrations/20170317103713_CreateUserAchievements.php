<?php
use Migrations\AbstractMigration;

class CreateUserAchievements extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_achievements');
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
		$table->addForeignKey("user_id","users","id");
		
        $table->addColumn('achievement_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
		$table->addForeignKey("achievement_id","achievements","id");
        $table->create();
    }
}
