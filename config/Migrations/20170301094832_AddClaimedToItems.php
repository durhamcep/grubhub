<?php
use Migrations\AbstractMigration;

class AddClaimedToItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('items');
        $table->addColumn('claimed', 'boolean', [
            'null' => false,
            'default' => false
        ]);
        $table->update();
    }
}
