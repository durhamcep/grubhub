<?php
use Migrations\AbstractMigration;

class ChangeReservedByNullable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('items');
        $table->changeColumn('reserved_by', 'integer',
                             ['default' => null,
                              'null' => true]);
        $table->addForeignKey('reserved_by', 'users', 'id');
    }
}
